## 组件亮点功能
- 👍 渲染引擎和业务代码分离、解耦。通过mono repo 的方式构建, 独立更新，不影响业务代码。
- 👍 采用游戏开发中的双缓冲技巧，优化和缓解拖动或缩放时渲染卡顿、跳帧问题。
- 👍 画布会根据父元素的大小，动态自适应变化，简化业务层使用。
- 👍 点位和区域离开画布即释放，优化内存和渲染效率。
- 👍 优化Retina屏显示效果，高清不模糊。
- 👍 支持React,Vue 和 纯js 方案。

## 预览效果
区域标注
![img.png](doc/imgs/img.png)

点位标注
![img_1.png](doc/imgs/img_1.png)

## 一、安装和使用
已上传到npm仓库，可以使用npm 和 yarn 安装
```
npm i @space-marker/core --save
```

or

```
yarn add @space-marker/core
```

## 二、初始化
初始化时，需要为画布创建一个父元素，画布会根据父元素的大小，自动创建合适的大小。
font-family: 'anticon';是必须的，否则字体图标第一次无法正常渲染

```
  <div class="map" style="background-color:#000;margin:0 auto;width: 1200px; height: 600px;outline: 1px solid red;position: relative;font-family: 'anticon';">
    &nbsp;
  </div>
```

引入二维标点库，进行相关操作
```
import {SpaceMarker} from '@space-marker/core'
```



## 三、创建画布
```
let el = document.querySelector('.map')
let canvas = new SpaceMarker(el, {
	...options // 初始化参数
})
```

画布初始化参数说明
```
{
    mapWidth:0,   // 图纸宽度（可以不传，会更加图纸大小计算）
    mapHeight: 0, // 图纸高度（可以不传，会更加图纸大小计算）
	scaleX: 1, // transform缩放点位修正
    scaleY: 1, // transform缩放点位修正
	mapImgUrl:''， // 图纸URL
    on: (event:string, data:any)=>{}, // 监听事件
    marks: [], // 位置落点
    ranges:[], // 区域落点
	editModel:null, // 是否直接进入编辑模式。'mark': 点位标注, 'range':区域标注（待开发）
	editOption:null, // 编辑模式附加参数。editModel='mark' 传点位的参数，editModel='range'时传区域参数，可改变图标，颜色，大小等
}
```

位置落点参数说明
```
{
	id: 唯一标识，可以自定义，不传系统自动生成
    center: {x: 0, y: 0}, // 落点坐标
	w: 画布宽度（兼容企业的现在的落点）
	h:  画布高度（兼容企业的现在的落点）
    ext: {}, // 扩展参数
    radius: 18, // 默认半径
    padding: 9,// 中心圆边距
	visible: true,//是否可见
	highlight: false, // 是否显示高亮涟漪
    highlightSize: 9, // 涟漪的大小
    icon: 'device_normal', //device_abnormal,device_normal,device_offline,device_alert,device_video,position, 默认的6套图标，还可以传字体图标样式或者图片的URL 
    bgColor: 'blue'        //blue,red,orange,yellow  内置的4套颜色，可以是其他自定义颜色
}

```

区域落点说明

```
{
    id: 唯一标识，可以自定义，不传系统自动生成
    ext: {},  // 扩展参数
    points:[],  // 区域点位
	w: 画布宽度（兼容企业的现在的落点）
	h:  画布高度（兼容企业的现在的落点）
    bgColor: 'blue'        //blue,red,orange,yellow 。 默认背景色
}
```


## 四、重新初始化
reset 方法，参数和创建时一样

```
canvas.reset({
	...options
})
```


## 五、事件监听
on接收回调函数

```
let canvas = new SpaceMarker(el, {
   ...
    on:(event,data) =>{
        console.log(event, data);
    }
})
```

event:枚举

mark_click: 点位点击事件
mark_enter:  点位鼠标悬浮事件
mark_leave: 点位鼠标移出事件

range_click: 区域点击事件
range_enter:  区域鼠标悬浮事件
range_leave: 区域鼠标移出事件

loaded: 初始化完成事件
render: 画布重新渲染事件回调

mark_pos: 标注点位成功事件


## 六、更新落点信息
#### 1. 更新位置点位

```
public updateMark(id, options, path: string = 'id')
```

id:查询的值，
options: 更新的参数
{
icon,
bgColor,
ext,highlight,
highlightSize,
visible,
}
path: 查询值，所在路径


##### DEMO
```
 canvas.updateMark('2', {
        bgColor: (['blue','red','orange','yellow'])[Math.round(Math.random()*10) % 4],
		ext:{},
        icon: (['L_device_type_005_001_001','device_abnormal','device_normal','device_offline','device_alert','device_video','position','L_device_type_003_001_001','L_device_type_004_004_006'])[Math.round(Math.random()*10) % 9],
    },'ext.index')
```


#### 2.更新区域
```
public updateRange(id, options, path: string = 'id')
```
id:查询的值，
options: 更新的参数
{
bgColor,
ext,
visible,
}
path: 查询值，所在路径

## 七、更新画布信息
```
public update(options)
```
可选参数
scaleX:
scaleY:

## 八、批量设置区域和落点的辅助方法
```
	// 高亮Mark
    public setMarkHighlight(id, path: string = 'id')
```
可选参数
id 可以是数组，为空则清除所有涟漪效果
path 是id的路径


```
	// 显示Mark
    public setMarkVisible(id, path: string = 'id')
```
可选参数
id 可以是单个元素的id值，显示某一个；可以是数组，批量显示；可以为空，则显示所有；可以是bool值，true=都显示，false=都隐藏
path 是id的路径

## 九、编辑模式
##### 编辑模式有两种进入方式:

##### 第一种
初始化时传入editModel，editModel 参数
- editModel='mark'          点位标注
- editModel='range'         区域标注（待开发）

- editOption  编辑模式附加参数。editModel='mark' 传点位的参数，editModel='range'时传区域参数，可改变图标，颜色，大小等

##### 第二种
创建完画布以后，调用

```
    // 设置编辑模式
    public setEditable(type, options)
```
- type: 编辑模式，可选的值为: 'mark', 'range'。 为空则退出编辑模式。
- options: 编辑模式附加参数（用于业务自定义颜色，大小等属性）