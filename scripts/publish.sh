#!/usr/bin/env bash

rm -rf dist/core

yarn build

cd dist/core && npm publish

