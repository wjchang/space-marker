let el = document.querySelector('.map')

let canvas = new BwwlSpaceMarker.SpaceMarker(el, {
    // scaleX: 0.424479,
    // scaleY: 0.424479,
    resizeMode: 'coverRect', // coverRect,fitRectHeight
    mapImgUrl: 'http://bwwl-platform.oss-cn-shanghai.aliyuncs.com/ali_oss/5f093425e138235840119745_res_file.png',
    marks: [{
        'center': { 'x': '175', 'y': '293' },
        'bgColor': '#F09614',
        'icon': 'L_device_type_001_001_002_009',
        'w': '1000',
        'h': '707',
        fit: true,
        ext: {
            index: '2'
        }
    },
        {
            'center': { 'x': '275', 'y': '293' },
            'bgColor': '#F09614',
            'icon': 'L_device_type_001_001_002_009',
            'w': '1000',
            'h': '707',
            fit: true,
            ext: {
                index: '3'
            }
        }
    ],

    on: (event, data) => {
        console.log(event, data)
    }
})

let re_init = document.getElementById('re_init')
re_init.onclick = () => {
    canvas.reset({
        mapImgUrl: 'http://web.cdn.cetcsafe.com/banner.png',
        on: (event, data) => {
            console.log(event, data)
        }
    })
}


let re_init2 = document.getElementById('re_init2')
re_init2.onclick = () => {
    canvas.reset({
        // resizeMode: 'coverRect', // coverRect,fitRectHeight
        mapImgUrl: 'http://bwwl-platform.oss-cn-shanghai.aliyuncs.com/ali_oss/5f093425e138235840119745_res_file.png',
        marks: [{
            'center': { 'x': '175', 'y': '293' },
            'bgColor': '#F09614',
            'icon': 'L_device_type_001_001_002_009',
            'w': '1000',
            'h': '707',
            fit: true,
            ext: {
                index: '2'
            }
        },
            {
                'center': { 'x': '275', 'y': '293' },
                'bgColor': '#F09614',
                'icon': 'L_device_type_001_001_002_009',
                'w': '1000',
                'h': '707',
                fit: true,
                ext: {
                    index: '3'
                }
            }
        ],
    })
}

let re_update_mark = document.getElementById('re_update_mark')
re_update_mark.onclick = () => {
    canvas.updateMark('2', {
        fit: true,
        bgColor: (['blue', 'red', 'orange', 'yellow'])[Math.round(Math.random() * 10) % 4],
        icon: (['L_device_type_005_001_001', 'device_abnormal', 'device_normal', 'device_offline', 'device_alert', 'device_video', 'position', 'L_device_type_003_001_001', 'L_device_type_004_004_006'])[Math.round(Math.random() * 10) % 9],
    }, 'ext.index')
}

// canvas.loadMapImg('http://bwwl-platform-cetc.oss-cn-hangzhou.aliyuncs.com/ali_oss/5eaa6a91adca681c111e5314_res_file.png')
