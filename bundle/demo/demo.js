let el = document.querySelector('.map')
let canvas = new BwwlSpaceMarker.SpaceMarker(el, {
    "mapImgUrl": "http://bwwl-platform.oss-cn-shanghai.aliyuncs.com/ali_oss/5f686f24e138236360908ffd_res_file.svg",
    "marks": [
        {
            "center": {
                "x": "930.5556",
                "y": "951.13873"
            },
            "bgColor": "blue",
            "icon": "L_device_type_001_004_003_014",
            "w": "1000.0",
            "h": "1000.0",
            "ext": {
                "vr_view_uuid": null,
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市蜀山区38所国防通道碧林园西87米",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-25 14:33:59",
                "offline_updated_at": null,
                "trans_code": "",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "4,5",
                    "system_code": "system_type_012",
                    "id": 716,
                    "entry_model_code": "model_code_0003",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "单点设备故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "12",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "单点设备故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "13",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "单点故障",
                                "selected": "1",
                                "identifier": "prop_single",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "WARNING",
                                            "eventText": "预警",
                                            "id": 2009,
                                            "name": "火灾探测设备预警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "3",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "火灾预报警",
                                "selected": "1",
                                "identifier": "prop_fire",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD",
                                            "eventText": "屏蔽",
                                            "id": 2005,
                                            "name": "消防探测报警联动设备屏蔽",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "18",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD_REALSE",
                                            "eventText": "屏蔽解除",
                                            "id": 2006,
                                            "name": "消防探测报警联动设备屏蔽解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "19",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "屏蔽",
                                "selected": "1",
                                "identifier": "prop_insulate",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "8",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "9",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "回路",
                                "selected": "1",
                                "identifier": "prop_loop",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备自动启动",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "64",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备自动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "65",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备自动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "66",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "自动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "67",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "OPEN_DELAY",
                                            "eventText": "延迟开启",
                                            "id": 2016,
                                            "name": "联动设备延迟开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "68",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备控制盘手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "69",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备控制盘手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "70",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备控制盘手动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "71",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "控制盘手动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "72",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备现场手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "73",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "现场手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "74",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "开关",
                                "selected": "1",
                                "identifier": "prop_on_off",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK",
                                            "eventText": "反馈",
                                            "id": 2019,
                                            "name": "联动设备反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "75",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK_RESET",
                                            "eventText": "反馈撤销",
                                            "id": 2032,
                                            "name": "反馈撤销",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "76",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "NO_RESPONSE",
                                            "eventText": "无响应",
                                            "id": 2022,
                                            "name": "无反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "77",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "反馈",
                                "selected": "1",
                                "identifier": "prop_feedback",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "RELATE",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "recovery",
                                            "eventCode": "RESET",
                                            "eventText": "复位",
                                            "id": 2002,
                                            "name": "主机复位",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "14",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "复位",
                                "identifier": "reset",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "LOCAL",
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "eventText": "巡检正常",
                                            "id": 2040,
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "巡检",
                                "identifier": "P0001",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S01_01)",
                                "resultDesc": "",
                                "condition_desc": "火灾预警(火灾预警)",
                                "resultId": [
                                    "S03_003",
                                    "S03_003_01"
                                ],
                                "result": "S03_003_01_01"
                            },
                            {
                                "condition": "(S03_01)",
                                "resultDesc": "",
                                "condition_desc": "单点故障(故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01"
                                ],
                                "result": "S03_002_01_03"
                            },
                            {
                                "condition": "(S02_01)",
                                "resultDesc": "",
                                "condition_desc": "屏蔽(屏蔽)",
                                "resultId": [
                                    "S03_005"
                                ],
                                "result": "S03_005_03"
                            },
                            {
                                "condition": "(S04_01)",
                                "resultDesc": "",
                                "condition_desc": "启动(启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_01"
                            },
                            {
                                "condition": "(S04_03)",
                                "resultDesc": "",
                                "condition_desc": "启动(延迟启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_02"
                            },
                            {
                                "condition": "(S05_01)",
                                "resultDesc": "",
                                "condition_desc": "反馈(反馈)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_03"
                            },
                            {
                                "condition": "(S06_01)",
                                "resultDesc": "",
                                "condition_desc": "离线(离线)",
                                "resultId": [
                                    "S03_001",
                                    "S03_001_01"
                                ],
                                "result": "S03_001_01"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002"
                                ],
                                "result": "S03_002_07"
                            }
                        ]
                    },
                    "icon_level": 4,
                    "is_edit": 0,
                    "parent_type_code": "device_type_001_004_003",
                    "image_files": [

                    ],
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "火灾预警",
                                "_index": 0,
                                "identifier": "state_fire",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "火灾预警",
                                                "typeCode": "7",
                                                "code": "S01_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2009,
                                                    "propId": "prop_fire",
                                                    "eventCode": "WARNING",
                                                    "eventText": "预警"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无火灾预警",
                                                "typeCode": "15",
                                                "code": "S01_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 371
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "屏蔽",
                                "_index": 1,
                                "identifier": "state_insulate",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "屏蔽",
                                                "typeCode": "9",
                                                "code": "S02_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2005,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD",
                                                    "eventText": "屏蔽"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无屏蔽",
                                                "typeCode": "15",
                                                "code": "S02_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2006,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD_REALSE",
                                                    "eventText": "屏蔽解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 385
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "单点故障",
                                "_index": 2,
                                "identifier": "state_single",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "故障",
                                                "typeCode": "5",
                                                "code": "S03_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                },
                                                {
                                                    "eventId": 2018,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "FAIL",
                                                    "eventText": "开启或关闭失败"
                                                },
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无故障",
                                                "typeCode": "15",
                                                "code": "S03_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 291
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "启动",
                                "_index": 3,
                                "identifier": "state_on",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "关闭",
                                                "typeCode": "8",
                                                "code": "S04_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2017,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "STOP",
                                                    "eventText": "关闭（联动）"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                },
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "延迟启动",
                                                "typeCode": "8",
                                                "code": "S04_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2016,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "OPEN_DELAY",
                                                    "eventText": "延迟开启"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "启动",
                                                "typeCode": "8",
                                                "code": "S04_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2015,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "START",
                                                    "eventText": "启动"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 123
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "反馈",
                                "_index": 4,
                                "identifier": "state_feedback",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "反馈",
                                                "typeCode": "8",
                                                "code": "S05_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2019,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK",
                                                    "eventText": "反馈"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "反馈撤销",
                                                "typeCode": "8",
                                                "code": "S05_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2032,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK_RESET",
                                                    "eventText": "反馈撤销"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无反馈",
                                                "typeCode": "8",
                                                "code": "S05_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "复位",
                                                "typeCode": "15",
                                                "code": "S05_04"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 265
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S92_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 4,
                    "device_type_name": "安全出口",
                    "is_end": 1,
                    "icon_prefix": "device_type_001_004_003",
                    "device_type_code": "device_type_001_004_003_014"
                },
                "extra_info": {

                },
                "parent_channel": null,
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe58788e13823402523cb79_device",
                "scrap_date": null,
                "vr_scene_id": null,
                "product_date": null,
                "device_model": "device_model_001_004_003_014",
                "mark_range": {
                    "x": "930.5556",
                    "w": "1000.0",
                    "y": "951.13873",
                    "h": "1000.0",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": null,
                "data_updated_at": "2020-12-25 14:32:40",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_001_004_003_014",
                "latitude": "31.859268463252484",
                "is_delete": 0,
                "longtitude": "117.2474474946429",
                "installation_date": "2020-12-25 08:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-26 15:11:27",
                "model_id": "",
                "device_name": "安全出口",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":\"F7\",\"x\":\"1.3052626498484375E7\",\"y\":\"3745695.0195234376\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "",
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 303.891,
                "y": 181.137
            },
            "bgColor": "blue",
            "icon": "L_device_type_001_006_002",
            "w": "1000",
            "h": "764",
            "ext": {
                "vr_view_uuid": null,
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市蜀山区38所国防通道碧林园西95米",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-25 14:32:55",
                "offline_updated_at": null,
                "trans_code": "",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "4,5",
                    "system_code": "system_type_014",
                    "id": 367,
                    "entry_model_code": "model_code_0003",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "单点设备故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "12",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "单点设备故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "13",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "单点故障",
                                "selected": "1",
                                "identifier": "prop_single",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "WARNING",
                                            "eventText": "预警",
                                            "id": 2009,
                                            "name": "火灾探测设备预警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "3",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "火灾预报警",
                                "selected": "1",
                                "identifier": "prop_fire",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD",
                                            "eventText": "屏蔽",
                                            "id": 2005,
                                            "name": "消防探测报警联动设备屏蔽",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "18",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD_REALSE",
                                            "eventText": "屏蔽解除",
                                            "id": 2006,
                                            "name": "消防探测报警联动设备屏蔽解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "19",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "屏蔽",
                                "selected": "1",
                                "identifier": "prop_insulate",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "8",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "9",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "回路",
                                "selected": "1",
                                "identifier": "prop_loop",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备自动启动",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "64",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备自动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "65",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备自动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "66",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "自动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "67",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "OPEN_DELAY",
                                            "eventText": "延迟开启",
                                            "id": 2016,
                                            "name": "联动设备延迟开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "68",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备控制盘手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "69",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备控制盘手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "70",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备控制盘手动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "71",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "控制盘手动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "72",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备现场手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "73",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "现场手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "74",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "开关",
                                "selected": "1",
                                "identifier": "prop_on_off",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK",
                                            "eventText": "反馈",
                                            "id": 2019,
                                            "name": "联动设备反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "75",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK_RESET",
                                            "eventText": "反馈撤销",
                                            "id": 2032,
                                            "name": "反馈撤销",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "76",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "NO_RESPONSE",
                                            "eventText": "无响应",
                                            "id": 2022,
                                            "name": "无反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "77",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "反馈",
                                "selected": "1",
                                "identifier": "prop_feedback",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "RELATE",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "recovery",
                                            "eventCode": "RESET",
                                            "eventText": "复位",
                                            "id": 2002,
                                            "name": "主机复位",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "14",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "复位",
                                "identifier": "reset",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "LOCAL",
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "eventText": "巡检正常",
                                            "id": 2040,
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "巡检",
                                "identifier": "P0001",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S01_01)",
                                "resultDesc": "",
                                "condition_desc": "火灾预警(火灾预警)",
                                "resultId": [
                                    "S03_003",
                                    "S03_003_01"
                                ],
                                "result": "S03_003_01_01"
                            },
                            {
                                "condition": "(S03_01)",
                                "resultDesc": "",
                                "condition_desc": "单点故障(故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01"
                                ],
                                "result": "S03_002_01_03"
                            },
                            {
                                "condition": "(S02_01)",
                                "resultDesc": "",
                                "condition_desc": "屏蔽(屏蔽)",
                                "resultId": [
                                    "S03_005"
                                ],
                                "result": "S03_005_03"
                            },
                            {
                                "condition": "(S04_01)",
                                "resultDesc": "",
                                "condition_desc": "启动(启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_01"
                            },
                            {
                                "condition": "(S04_03)",
                                "resultDesc": "",
                                "condition_desc": "启动(延迟启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_02"
                            },
                            {
                                "condition": "(S05_01)",
                                "resultDesc": "",
                                "condition_desc": "反馈(反馈)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_03"
                            },
                            {
                                "condition": "(S06_01)",
                                "resultDesc": "",
                                "condition_desc": "离线(离线)",
                                "resultId": [
                                    "S03_001",
                                    "S03_001_01"
                                ],
                                "result": "S03_001_01"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002"
                                ],
                                "result": "S03_002_07"
                            }
                        ]
                    },
                    "icon_level": 3,
                    "is_edit": 0,
                    "parent_type_code": "device_type_001_006_002",
                    "image_files": [

                    ],
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "火灾预警",
                                "_index": 0,
                                "identifier": "state_fire",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "火灾预警",
                                                "typeCode": "7",
                                                "code": "S01_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2009,
                                                    "propId": "prop_fire",
                                                    "eventCode": "WARNING",
                                                    "eventText": "预警"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无火灾预警",
                                                "typeCode": "15",
                                                "code": "S01_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 371
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "屏蔽",
                                "_index": 1,
                                "identifier": "state_insulate",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "屏蔽",
                                                "typeCode": "9",
                                                "code": "S02_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2005,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD",
                                                    "eventText": "屏蔽"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无屏蔽",
                                                "typeCode": "15",
                                                "code": "S02_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2006,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD_REALSE",
                                                    "eventText": "屏蔽解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 385
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "单点故障",
                                "_index": 2,
                                "identifier": "state_single",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "故障",
                                                "typeCode": "5",
                                                "code": "S03_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                },
                                                {
                                                    "eventId": 2018,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "FAIL",
                                                    "eventText": "开启或关闭失败"
                                                },
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无故障",
                                                "typeCode": "15",
                                                "code": "S03_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 291
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "启动",
                                "_index": 3,
                                "identifier": "state_on",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "关闭",
                                                "typeCode": "8",
                                                "code": "S04_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2017,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "STOP",
                                                    "eventText": "关闭（联动）"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                },
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "延迟启动",
                                                "typeCode": "8",
                                                "code": "S04_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2016,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "OPEN_DELAY",
                                                    "eventText": "延迟开启"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "启动",
                                                "typeCode": "8",
                                                "code": "S04_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2015,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "START",
                                                    "eventText": "启动"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 123
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "反馈",
                                "_index": 4,
                                "identifier": "state_feedback",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "反馈",
                                                "typeCode": "8",
                                                "code": "S05_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2019,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK",
                                                    "eventText": "反馈"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "反馈撤销",
                                                "typeCode": "8",
                                                "code": "S05_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2032,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK_RESET",
                                                    "eventText": "反馈撤销"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无反馈",
                                                "typeCode": "8",
                                                "code": "S05_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "复位",
                                                "typeCode": "15",
                                                "code": "S05_04"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 265
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S92_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 4,
                    "device_type_name": "A类防火门",
                    "is_end": 1,
                    "icon_prefix": "device_type_001_006_002",
                    "device_type_code": "device_type_001_006_002_001"
                },
                "extra_info": {

                },
                "parent_channel": null,
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe58748e13823402b3a20a1_device",
                "scrap_date": null,
                "vr_scene_id": null,
                "product_date": null,
                "device_model": "device_model_001_006_002_001",
                "mark_range": {
                    "x": "609",
                    "w": "1000",
                    "y": "363",
                    "h": "764",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": null,
                "data_updated_at": "2020-12-25 14:31:36",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_001_006_002_001",
                "latitude": "31.859350467006347",
                "is_delete": 0,
                "longtitude": "117.2473834941293",
                "installation_date": "2020-12-25 00:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-26 15:08:40",
                "model_id": "",
                "device_name": "A类防火门",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":1,\"x\":\"\",\"y\":\"\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "",
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 46.407,
                "y": 38.922
            },
            "bgColor": "blue",
            "icon": "L_device_type_003_001_001",
            "w": "1000",
            "h": "764",
            "ext": {
                "vr_view_uuid": null,
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市蜀山区38所国防通道碧林园西83米",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-25 13:56:15",
                "offline_updated_at": null,
                "trans_code": "",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "4,5",
                    "system_code": "system_type_017",
                    "id": 604,
                    "entry_model_code": "model_code_0003",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "单点设备故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "12",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "单点设备故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "13",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "单点故障",
                                "selected": "1",
                                "identifier": "prop_single",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "WARNING",
                                            "eventText": "预警",
                                            "id": 2009,
                                            "name": "火灾探测设备预警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "3",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "火灾预报警",
                                "selected": "1",
                                "identifier": "prop_fire",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD",
                                            "eventText": "屏蔽",
                                            "id": 2005,
                                            "name": "消防探测报警联动设备屏蔽",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "18",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD_REALSE",
                                            "eventText": "屏蔽解除",
                                            "id": 2006,
                                            "name": "消防探测报警联动设备屏蔽解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "19",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "屏蔽",
                                "selected": "1",
                                "identifier": "prop_insulate",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "8",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "9",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "回路",
                                "selected": "1",
                                "identifier": "prop_loop",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备自动启动",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "64",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备自动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "65",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备自动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "66",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "自动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "67",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "OPEN_DELAY",
                                            "eventText": "延迟开启",
                                            "id": 2016,
                                            "name": "联动设备延迟开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "68",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备控制盘手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "69",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备控制盘手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "70",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备控制盘手动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "71",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "控制盘手动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "72",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备现场手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "73",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "现场手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "74",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "开关",
                                "selected": "1",
                                "identifier": "prop_on_off",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK",
                                            "eventText": "反馈",
                                            "id": 2019,
                                            "name": "联动设备反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "75",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK_RESET",
                                            "eventText": "反馈撤销",
                                            "id": 2032,
                                            "name": "反馈撤销",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "76",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "NO_RESPONSE",
                                            "eventText": "无响应",
                                            "id": 2022,
                                            "name": "无反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "77",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "反馈",
                                "selected": "1",
                                "identifier": "prop_feedback",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "RELATE",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "recovery",
                                            "eventCode": "RESET",
                                            "eventText": "复位",
                                            "id": 2002,
                                            "name": "主机复位",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "14",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "复位",
                                "identifier": "reset",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "LOCAL",
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "eventText": "巡检正常",
                                            "id": 2040,
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "巡检",
                                "identifier": "P0001",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S01_01)",
                                "resultDesc": "",
                                "condition_desc": "火灾预警(火灾预警)",
                                "resultId": [
                                    "S03_003",
                                    "S03_003_01"
                                ],
                                "result": "S03_003_01_01"
                            },
                            {
                                "condition": "(S03_01)",
                                "resultDesc": "",
                                "condition_desc": "单点故障(故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01"
                                ],
                                "result": "S03_002_01_03"
                            },
                            {
                                "condition": "(S02_01)",
                                "resultDesc": "",
                                "condition_desc": "屏蔽(屏蔽)",
                                "resultId": [
                                    "S03_005"
                                ],
                                "result": "S03_005_03"
                            },
                            {
                                "condition": "(S04_01)",
                                "resultDesc": "",
                                "condition_desc": "启动(启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_01"
                            },
                            {
                                "condition": "(S04_03)",
                                "resultDesc": "",
                                "condition_desc": "启动(延迟启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_02"
                            },
                            {
                                "condition": "(S05_01)",
                                "resultDesc": "",
                                "condition_desc": "反馈(反馈)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_03"
                            },
                            {
                                "condition": "(S06_01)",
                                "resultDesc": "",
                                "condition_desc": "离线(离线)",
                                "resultId": [
                                    "S03_001",
                                    "S03_001_01"
                                ],
                                "result": "S03_001_01"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002"
                                ],
                                "result": "S03_002_07"
                            }
                        ]
                    },
                    "icon_level": 3,
                    "is_edit": 0,
                    "parent_type_code": "device_type_003_001_001",
                    "image_files": null,
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "火灾预警",
                                "_index": 0,
                                "identifier": "state_fire",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "火灾预警",
                                                "typeCode": "7",
                                                "code": "S01_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2009,
                                                    "propId": "prop_fire",
                                                    "eventCode": "WARNING",
                                                    "eventText": "预警"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无火灾预警",
                                                "typeCode": "15",
                                                "code": "S01_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 371
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "屏蔽",
                                "_index": 1,
                                "identifier": "state_insulate",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "屏蔽",
                                                "typeCode": "9",
                                                "code": "S02_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2005,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD",
                                                    "eventText": "屏蔽"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无屏蔽",
                                                "typeCode": "15",
                                                "code": "S02_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2006,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD_REALSE",
                                                    "eventText": "屏蔽解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 385
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "单点故障",
                                "_index": 2,
                                "identifier": "state_single",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "故障",
                                                "typeCode": "5",
                                                "code": "S03_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                },
                                                {
                                                    "eventId": 2018,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "FAIL",
                                                    "eventText": "开启或关闭失败"
                                                },
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无故障",
                                                "typeCode": "15",
                                                "code": "S03_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 291
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "启动",
                                "_index": 3,
                                "identifier": "state_on",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "关闭",
                                                "typeCode": "8",
                                                "code": "S04_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2017,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "STOP",
                                                    "eventText": "关闭（联动）"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                },
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "延迟启动",
                                                "typeCode": "8",
                                                "code": "S04_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2016,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "OPEN_DELAY",
                                                    "eventText": "延迟开启"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "启动",
                                                "typeCode": "8",
                                                "code": "S04_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2015,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "START",
                                                    "eventText": "启动"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 123
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "反馈",
                                "_index": 4,
                                "identifier": "state_feedback",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "反馈",
                                                "typeCode": "8",
                                                "code": "S05_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2019,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK",
                                                    "eventText": "反馈"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "反馈撤销",
                                                "typeCode": "8",
                                                "code": "S05_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2032,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK_RESET",
                                                    "eventText": "反馈撤销"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无反馈",
                                                "typeCode": "8",
                                                "code": "S05_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "复位",
                                                "typeCode": "15",
                                                "code": "S05_04"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 265
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S92_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 4,
                    "device_type_name": "料仓",
                    "is_end": 1,
                    "icon_prefix": "device_type_003_001_001",
                    "device_type_code": "device_type_003_001_001_001"
                },
                "extra_info": {

                },
                "parent_channel": null,
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe57eb0e138234025fe095a_device",
                "scrap_date": null,
                "vr_scene_id": null,
                "product_date": null,
                "device_model": "device_model_003_001_001_001",
                "mark_range": {
                    "x": "93",
                    "w": "1000",
                    "y": "78",
                    "h": "764",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": null,
                "data_updated_at": "2020-12-25 13:54:56",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_003_001_001_001",
                "latitude": "31.85900245991404",
                "is_delete": 0,
                "longtitude": "117.24752449484534",
                "installation_date": "2020-12-25 00:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-26 15:10:05",
                "model_id": "",
                "device_name": "料仓",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":1,\"x\":\"\",\"y\":\"\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "",
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 308.64075644999997,
                "y": 235.58140797
            },
            "bgColor": "blue",
            "icon": "L_device_type_002_001_001_001",
            "w": "1000.0",
            "h": "1000.0",
            "ext": {
                "vr_view_uuid": null,
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市蜀山区38所国防通道碧林园西105米",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-25 10:20:11",
                "offline_updated_at": null,
                "trans_code": "",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "2,4,5",
                    "system_code": "system_type_008",
                    "id": 592,
                    "entry_model_code": "model_code_0008",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "absense",
                                            "eventCode": "ABSENSE",
                                            "eventText": "人员离岗",
                                            "id": 2035,
                                            "name": "人员离岗报警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0011",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "work",
                                            "eventCode": "WORKING",
                                            "eventText": "工作进行",
                                            "id": 2033,
                                            "name": "离岗恢复",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0012",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "离岗检测",
                                "selected": "1",
                                "identifier": "prop_absence",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "alert",
                                            "eventCode": "ALERT",
                                            "eventText": "火灾报警",
                                            "id": 2007,
                                            "name": "烟火识别报警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0009",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "recovery",
                                            "eventCode": "ALERT_REALSE",
                                            "isNormalType": "",
                                            "id": 2008,
                                            "eventText": "火灾报警解除",
                                            "name": "烟火识别报警恢复",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0010",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "烟火识别报警",
                                "selected": "1",
                                "identifier": "prop_smoke_alarm",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 0,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "dataabnor",
                                            "eventCode": "ABNORMAL",
                                            "eventText": "异常",
                                            "id": 2011,
                                            "name": "消防通道占用",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0015",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "recovery",
                                            "eventCode": "ABNORMAL_REALSE",
                                            "isNormalType": "",
                                            "id": 2012,
                                            "eventText": "无异常",
                                            "name": "消防通道未占用",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0016",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "通道占用",
                                "selected": "1",
                                "identifier": "prop_access_occupy",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 0,
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "isNormalType": "",
                                            "id": 2040,
                                            "eventText": "巡检正常",
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "巡检",
                                "selected": "1",
                                "identifier": "P0001",
                                "type": "LOCAL",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S01_01)",
                                "resultDesc": "",
                                "condition_desc": "离岗检测(人员离岗)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_08"
                                ],
                                "result": "S03_002_08"
                            },
                            {
                                "condition": "(S01_02)",
                                "resultDesc": "",
                                "condition_desc": "离岗检测(离岗恢复)",
                                "resultId": [
                                    null
                                ],
                                "result": null
                            },
                            {
                                "condition": "(S02_01)",
                                "resultDesc": "",
                                "condition_desc": "烟火识别(烟火识别报警)",
                                "resultId": [
                                    "S03_003",
                                    "S03_003_07"
                                ],
                                "result": "S03_003_07"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_07"
                                ],
                                "result": "S03_002_07"
                            },
                            {
                                "condition": "(S031_01)",
                                "resultDesc": "",
                                "condition_desc": "通道占用(通道占用)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_09"
                                ],
                                "result": "S03_002_09"
                            }
                        ]
                    },
                    "icon_level": 4,
                    "is_edit": 0,
                    "parent_type_code": "device_type_002_001_001_001",
                    "image_files": [

                    ],
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "人员离岗",
                                                "typeCode": "10",
                                                "code": "S01_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2035,
                                                    "propId": "prop_absence",
                                                    "eventCode": "ABSENSE",
                                                    "eventText": "人员离岗"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "离岗恢复",
                                                "typeCode": "10",
                                                "code": "S01_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2033,
                                                    "propId": "prop_absence",
                                                    "eventCode": "WORKING",
                                                    "eventText": "工作进行"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "离岗检测",
                                "identifier": "state_absence"
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "烟火识别报警",
                                                "typeCode": "6",
                                                "code": "S02_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2007,
                                                    "propId": "prop_smoke_alarm",
                                                    "eventCode": "ALERT",
                                                    "eventText": "火灾报警"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "烟火识别无报警",
                                                "typeCode": "15",
                                                "code": "S02_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2008,
                                                    "propId": "prop_smoke_alarm",
                                                    "eventCode": "ALERT_REALSE",
                                                    "eventText": "火灾报警解除"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "烟火识别",
                                "identifier": "state_smoke_alarm"
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "通道占用",
                                                "typeCode": "16",
                                                "code": "S031_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2011,
                                                    "propId": "prop_access_occupy",
                                                    "eventCode": "ABNORMAL",
                                                    "eventText": "异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "通道无占用",
                                                "typeCode": "15",
                                                "code": "S03_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2012,
                                                    "propId": "prop_access_occupy",
                                                    "eventCode": "ABNORMAL_REALSE",
                                                    "eventText": "无异常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "通道占用",
                                "identifier": "state_access_occupy"
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "巡检状态",
                                "_index": 3,
                                "identifier": "patrol_state",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S04_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S04_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 106
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S99_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 5,
                    "device_type_name": "可见光半球形摄像机",
                    "is_end": 1,
                    "icon_prefix": "device_type_002_001_001_001",
                    "device_type_code": "device_type_002_001_001_001_002"
                },
                "extra_info": {
                    "video_info": {
                        "access_way_code": "video_mode_001",
                        "is_fire_control": "1"
                    }
                },
                "parent_channel": null,
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe54c0ce138231cd3235b39_device",
                "scrap_date": null,
                "vr_scene_id": null,
                "product_date": null,
                "device_model": "device_model_002_001_001_001_002",
                "mark_range": {
                    "x": "618.51855",
                    "w": "1000.0",
                    "y": "472.10703",
                    "h": "1000.0",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": null,
                "data_updated_at": "2020-12-25 10:18:52",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_002_001_001_001_002",
                "latitude": "31.85899947392856",
                "is_delete": 0,
                "longtitude": "117.2473154926165",
                "installation_date": "2020-12-25 08:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-25 11:12:03",
                "model_id": "",
                "device_name": "可见光半球形摄像机",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":\"F7\",\"x\":\"1.3052621134109374E7\",\"y\":\"3745673.0659863283\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "",
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 226.86019036000002,
                "y": 243.44842739999999
            },
            "bgColor": "blue",
            "icon": "L_device_type_001_001_002_002",
            "w": "1000.0",
            "h": "1000.0",
            "ext": {
                "vr_view_uuid": null,
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市庐阳区三孝口街道蒙城路徽商·国际大厦",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-24 18:11:58",
                "offline_updated_at": null,
                "trans_code": "01_01_001",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "4,5",
                    "system_code": "system_type_001",
                    "id": 21,
                    "entry_model_code": "model_code_0003",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "单点设备故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "12",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "单点设备故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "13",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "单点故障",
                                "selected": "1",
                                "identifier": "prop_single",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "WARNING",
                                            "eventText": "预警",
                                            "id": 2009,
                                            "name": "火灾探测设备预警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "3",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "火灾预报警",
                                "selected": "1",
                                "identifier": "prop_fire",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD",
                                            "eventText": "屏蔽",
                                            "id": 2005,
                                            "name": "消防探测报警联动设备屏蔽",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "18",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "SHIELD_REALSE",
                                            "eventText": "屏蔽解除",
                                            "id": 2006,
                                            "name": "消防探测报警联动设备屏蔽解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "19",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "屏蔽",
                                "selected": "1",
                                "identifier": "prop_insulate",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "8",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "9",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "回路",
                                "selected": "1",
                                "identifier": "prop_loop",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备自动启动",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "64",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备自动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "65",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备自动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "66",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "自动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "67",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "OPEN_DELAY",
                                            "eventText": "延迟开启",
                                            "id": 2016,
                                            "name": "联动设备延迟开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "68",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备控制盘手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "69",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "联动设备控制盘手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "70",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "联动设备控制盘手动开启失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "71",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "FAIL",
                                            "eventText": "开启或关闭失败",
                                            "id": 2018,
                                            "name": "控制盘手动关闭失败",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "72",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "START",
                                            "eventText": "启动",
                                            "id": 2015,
                                            "name": "联动设备现场手动开启",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "73",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "linkage",
                                            "eventCode": "STOP",
                                            "eventText": "关闭（联动）",
                                            "id": 2017,
                                            "name": "现场手动关闭",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "74",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "开关",
                                "selected": "1",
                                "identifier": "prop_on_off",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK",
                                            "eventText": "反馈",
                                            "id": 2019,
                                            "name": "联动设备反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "75",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "FEEDBACK_RESET",
                                            "eventText": "反馈撤销",
                                            "id": 2032,
                                            "name": "反馈撤销",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "76",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": null,
                                            "eventCode": "NO_RESPONSE",
                                            "eventText": "无响应",
                                            "id": 2022,
                                            "name": "无反馈",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "77",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "反馈",
                                "selected": "1",
                                "identifier": "prop_feedback",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "RELATE",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "recovery",
                                            "eventCode": "RESET",
                                            "eventText": "复位",
                                            "id": 2002,
                                            "name": "主机复位",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "14",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "复位",
                                "identifier": "reset",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "LOCAL",
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "eventText": "巡检正常",
                                            "id": 2040,
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "巡检",
                                "identifier": "P0001",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S01_01)",
                                "resultDesc": "",
                                "condition_desc": "火灾预警(火灾预警)",
                                "resultId": [
                                    "S03_003",
                                    "S03_003_01"
                                ],
                                "result": "S03_003_01_01"
                            },
                            {
                                "condition": "(S03_01)",
                                "resultDesc": "",
                                "condition_desc": "单点故障(故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01"
                                ],
                                "result": "S03_002_01_03"
                            },
                            {
                                "condition": "(S02_01)",
                                "resultDesc": "",
                                "condition_desc": "屏蔽(屏蔽)",
                                "resultId": [
                                    "S03_005"
                                ],
                                "result": "S03_005_03"
                            },
                            {
                                "condition": "(S04_01)",
                                "resultDesc": "",
                                "condition_desc": "启动(启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_01"
                            },
                            {
                                "condition": "(S04_03)",
                                "resultDesc": "",
                                "condition_desc": "启动(延迟启动)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_02"
                            },
                            {
                                "condition": "(S05_01)",
                                "resultDesc": "",
                                "condition_desc": "反馈(反馈)",
                                "resultId": [
                                    "S03_004"
                                ],
                                "result": "S03_004_03"
                            },
                            {
                                "condition": "(S06_01)",
                                "resultDesc": "",
                                "condition_desc": "离线(离线)",
                                "resultId": [
                                    "S03_001",
                                    "S03_001_01"
                                ],
                                "result": "S03_001_01"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002"
                                ],
                                "result": "S03_002_07"
                            }
                        ]
                    },
                    "icon_level": 4,
                    "is_edit": 0,
                    "parent_type_code": "device_type_001_001_002_002",
                    "image_files": [

                    ],
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "火灾预警",
                                "_index": 0,
                                "identifier": "state_fire",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "火灾预警",
                                                "typeCode": "7",
                                                "code": "S01_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2009,
                                                    "propId": "prop_fire",
                                                    "eventCode": "WARNING",
                                                    "eventText": "预警"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无火灾预警",
                                                "typeCode": "15",
                                                "code": "S01_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 371
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "屏蔽",
                                "_index": 1,
                                "identifier": "state_insulate",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "屏蔽",
                                                "typeCode": "9",
                                                "code": "S02_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2005,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD",
                                                    "eventText": "屏蔽"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无屏蔽",
                                                "typeCode": "15",
                                                "code": "S02_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2006,
                                                    "propId": "prop_insulate",
                                                    "eventCode": "SHIELD_REALSE",
                                                    "eventText": "屏蔽解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 385
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "单点故障",
                                "_index": 2,
                                "identifier": "state_single",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "故障",
                                                "typeCode": "5",
                                                "code": "S03_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                },
                                                {
                                                    "eventId": 2018,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "FAIL",
                                                    "eventText": "开启或关闭失败"
                                                },
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无故障",
                                                "typeCode": "15",
                                                "code": "S03_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_single",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 291
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "启动",
                                "_index": 3,
                                "identifier": "state_on",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "关闭",
                                                "typeCode": "8",
                                                "code": "S04_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2017,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "STOP",
                                                    "eventText": "关闭（联动）"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                },
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "延迟启动",
                                                "typeCode": "8",
                                                "code": "S04_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2016,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "OPEN_DELAY",
                                                    "eventText": "延迟开启"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "启动",
                                                "typeCode": "8",
                                                "code": "S04_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2015,
                                                    "propId": "prop_on_off",
                                                    "eventCode": "START",
                                                    "eventText": "启动"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 123
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "反馈",
                                "_index": 4,
                                "identifier": "state_feedback",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "反馈",
                                                "typeCode": "8",
                                                "code": "S05_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2019,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK",
                                                    "eventText": "反馈"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "反馈撤销",
                                                "typeCode": "8",
                                                "code": "S05_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2032,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "FEEDBACK_RESET",
                                                    "eventText": "反馈撤销"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无反馈",
                                                "typeCode": "8",
                                                "code": "S05_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2022,
                                                    "propId": "prop_feedback",
                                                    "eventCode": "NO_RESPONSE",
                                                    "eventText": "无响应"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "复位",
                                                "typeCode": "15",
                                                "code": "S05_04"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 265
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S92_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 5,
                    "device_type_name": "点型定温火灾探测器",
                    "is_end": 1,
                    "icon_prefix": "device_type_001_001_002_002",
                    "device_type_code": "device_type_001_001_002_002_001"
                },
                "extra_info": {

                },
                "parent_channel": null,
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe4691fe138231cd32a22d1_device",
                "scrap_date": null,
                "vr_scene_id": null,
                "product_date": null,
                "device_model": "device_model_001_001_002_002_001",
                "mark_range": {
                    "x": "454.62964",
                    "w": "1000.0",
                    "y": "487.8726",
                    "h": "1000.0",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": null,
                "data_updated_at": "2020-12-24 18:10:39",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_001_001_002_002_001",
                "latitude": "31.870149",
                "is_delete": 0,
                "longtitude": "117.278411",
                "installation_date": null,
                "model_type": "",
                "parent_id": "5fe05884e138235129b83c0f_device",
                "updated_at": "2020-12-25 13:54:10",
                "model_id": "",
                "device_name": "点型定温火灾探测仪",
                "model_name": "",
                "manufactor_id": null,
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":\"F7\",\"x\":\"1.305260005161621E7\",\"y\":\"3745702.393285156\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": null,
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 266.466,
                "y": 192.115
            },
            "bgColor": "blue",
            "icon": "L_device_type_004_002",
            "w": "1000",
            "h": "762.995",
            "ext": {
                "vr_view_uuid": "980e25ac0e8f138f",
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市瑶海区七里塘街道安徽省第二人民医院门诊",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-23 11:08:12",
                "offline_updated_at": null,
                "trans_code": "20201223001",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "20201223001",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "",
                    "system_code": "system_type_099",
                    "id": 690,
                    "entry_model_code": "model_code_0002",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "0",
                                "perm_type": 0,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {

                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "eventText": "",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "name": "",
                                            "eventCode": "",
                                            "type": ""
                                        }
                                    ]
                                },
                                "name": "故障",
                                "selected": "1",
                                "identifier": "prop_fault",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "selected": "1",
                                "type": "LOCAL",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "eventText": "巡检正常",
                                            "id": 2040,
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "巡检",
                                "identifier": "P0001",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002"
                                ],
                                "result": "S03_002_07"
                            }
                        ]
                    },
                    "icon_level": 2,
                    "is_edit": 0,
                    "parent_type_code": "device_type_004_002",
                    "image_files": [

                    ],
                    "status_config": {
                        "status": [
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S99_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 3,
                    "device_type_name": "有线物联网网关",
                    "is_end": 1,
                    "icon_prefix": "device_type_004_002",
                    "device_type_code": "device_type_004_002_001"
                },
                "extra_info": {

                },
                "parent_channel": null,
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe2b44de138232e101ca11e_device",
                "scrap_date": null,
                "vr_scene_id": "scene_78ab4b23aa680686",
                "product_date": null,
                "device_model": "BW-WG01A",
                "mark_range": {
                    "x": "534",
                    "w": "1000",
                    "y": "385",
                    "h": "762.995",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": "hs-1917970989",
                "data_updated_at": "2020-12-23 11:06:53",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_004_002_001",
                "latitude": "31.89522",
                "is_delete": 0,
                "longtitude": "117.30622",
                "installation_date": "2020-12-23 08:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-25 13:49:23",
                "model_id": "",
                "device_name": "7层未标点的设备",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":\"F7\",\"x\":\"1.305262703602539E7\",\"y\":\"3745669.9509453126\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "",
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 265.967,
                "y": 193.113
            },
            "bgColor": "blue",
            "icon": "L_device_type",
            "w": "1000",
            "h": "762.995",
            "ext": {
                "vr_view_uuid": "980e25ac0e8f138f",
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市庐阳区双岗街道沿河路环城公园(赵岗路)",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-22 09:43:51",
                "offline_updated_at": null,
                "trans_code": "HZLD8001",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 1,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "4,5,6",
                    "system_code": "system_type_009",
                    "id": 12,
                    "entry_model_code": "model_code_0009",
                    "profile": {
                        "properties": [
                            {
                                "type": "LOCAL",
                                "selected": "1",
                                "overwrite": "0",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "报警主机主电故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "4",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "报警主机主电故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "5",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "identifier": "prop_main_power",
                                "name": "主电",
                                "monitorType": 1
                            },
                            {
                                "type": "LOCAL",
                                "selected": "1",
                                "overwrite": "1",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "报警主机备电故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "6",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "报警主机备电故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "7",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "identifier": "prop_emergency_power",
                                "name": "备电",
                                "monitorType": 1
                            },
                            {
                                "type": "LOCAL",
                                "selected": "1",
                                "overwrite": "1",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "4",
                                            "eventCode": "MUTE",
                                            "eventText": "消音",
                                            "id": 2003,
                                            "name": "消音",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "15",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "identifier": "prop_silence",
                                "name": "蜂鸣开关",
                                "monitorType": 1
                            },
                            {
                                "type": "LOCAL",
                                "selected": "1",
                                "overwrite": "1",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "回路故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "8",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "-1"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "回路故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "9",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "-1"
                                        },
                                        {
                                            "type": "offline",
                                            "eventCode": "OFFLINE",
                                            "eventText": "离线",
                                            "id": 2013,
                                            "name": "用传串口故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "149",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "recovery",
                                            "eventCode": "OFFLINE_REALSE",
                                            "eventText": "离线解除",
                                            "id": 2014,
                                            "name": "用传串口故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "150",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "identifier": "prop_loop",
                                "name": "回路",
                                "monitorType": 1
                            },
                            {
                                "type": "LOCAL",
                                "selected": "1",
                                "overwrite": "1",
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "recovery",
                                            "eventCode": "RESET",
                                            "eventText": "复位",
                                            "id": 2002,
                                            "name": "主机复位",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "14",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "-1"
                                        }
                                    ]
                                },
                                "identifier": "host_reset",
                                "name": "复位",
                                "monitorType": 1
                            },
                            {
                                "type": "LOCAL",
                                "selected": "1",
                                "overwrite": "1",
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "isNormalType": "",
                                            "id": 2040,
                                            "eventText": "巡检正常",
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "identifier": "P0001",
                                "name": "巡检",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S31_02) && (S32_02) && (S33_02) && (S34_02) && (S34_04)",
                                "resultDesc": "",
                                "condition_desc": "主电(主电正常) && 备电(备电正常) && 消音(无消音) && 回路(故障解除) && 回路(离线恢复)",
                                "resultId": [
                                    null
                                ],
                                "result": null
                            },
                            {
                                "condition": "(S32_01) || (S31_01)",
                                "resultDesc": "",
                                "condition_desc": "备电(备电故障) || 主电(主电故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01",
                                    "S03_002_01_01"
                                ],
                                "result": "S03_002_01_01"
                            },
                            {
                                "condition": "(S34_01)",
                                "resultDesc": "",
                                "condition_desc": "回路(故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01",
                                    "S03_002_01_02"
                                ],
                                "result": "S03_002_01_02"
                            },
                            {
                                "condition": "(S34_03)",
                                "resultDesc": "",
                                "condition_desc": "回路(离线)",
                                "resultId": [
                                    "S03_001",
                                    "S03_001_01"
                                ],
                                "result": "S03_001_01"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_07"
                                ],
                                "result": "S03_002_07"
                            }
                        ]
                    },
                    "icon_level": 0,
                    "is_edit": 1,
                    "parent_type_code": "device_type_001_001_001",
                    "image_files": [
                        "5f04186be138233c9ad3b003_res_file"
                    ],
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "name": "主电",
                                "_index": 0,
                                "identifier": "state_main_power",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "主电故障",
                                                "typeCode": "5",
                                                "code": "S31_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_main_power",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "主电正常",
                                                "typeCode": "15",
                                                "code": "S31_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_main_power",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 199
                            },
                            {
                                "overwrite": "0",
                                "name": "备电",
                                "_index": 1,
                                "identifier": "state_emergency_power",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "备电故障",
                                                "typeCode": "5",
                                                "code": "S32_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_emergency_power",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "备电正常",
                                                "typeCode": "15",
                                                "code": "S32_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_emergency_power",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 213
                            },
                            {
                                "overwrite": "0",
                                "name": "消音",
                                "_index": 2,
                                "identifier": "state_silence",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "消音",
                                                "typeCode": "13",
                                                "code": "S33_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2003,
                                                    "propId": "prop_silence",
                                                    "eventCode": "MUTE",
                                                    "eventText": "消音"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无消音",
                                                "typeCode": "15",
                                                "code": "S33_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 241
                            },
                            {
                                "overwrite": "0",
                                "name": "回路",
                                "_index": 3,
                                "identifier": "state_loop",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "故障",
                                                "code": "S34_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "故障解除",
                                                "code": "S34_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "离线",
                                                "typeCode": "",
                                                "code": "S34_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2013,
                                                    "propId": "prop_loop",
                                                    "eventCode": "OFFLINE",
                                                    "eventText": "离线"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "离线恢复",
                                                "typeCode": "",
                                                "code": "S34_04"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2014,
                                                    "propId": "prop_loop",
                                                    "eventCode": "OFFLINE_REALSE",
                                                    "eventText": "离线解除"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 255
                            },
                            {
                                "overwrite": "0",
                                "name": "巡检状态",
                                "_index": 4,
                                "identifier": "patrol_state",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S35_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S35_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 189
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S99_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": ",Electrical fire",
                    "is_public": 0,
                    "device_type_level": 4,
                    "device_type_name": "电气火灾监控控制器",
                    "is_end": 1,
                    "icon_prefix": "device_type",
                    "device_type_code": "device_type_001_001_001_003"
                },
                "extra_info": {

                },
                "parent_channel": "1",
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe14f09e1382351231e59f8_device",
                "scrap_date": null,
                "vr_scene_id": "scene_9641c857ea941036",
                "product_date": null,
                "device_model": "HZLD800",
                "mark_range": {
                    "x": "533",
                    "w": "1000",
                    "y": "387",
                    "h": "762.995",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": "hs-1918211895",
                "data_updated_at": "2020-12-22 09:42:33",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_001_001_001_003",
                "latitude": "31.874668",
                "is_delete": 0,
                "longtitude": "117.280127",
                "installation_date": "2020-12-22 08:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-25 10:02:01",
                "model_id": "",
                "device_name": "电气火灾监控控制器",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":\"F7\",\"x\":\"1.3052649254765624E7\",\"y\":\"3745693.577980469\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "",
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 251.8101704,
                "y": 222.8731105
            },
            "bgColor": "blue",
            "icon": "L_device_type",
            "w": "1000.0",
            "h": "1000.0",
            "ext": {
                "vr_view_uuid": "980e25ac0e8f138f",
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市庐阳区三孝口街道蒙城路徽商·国际大厦",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-21 16:12:02",
                "offline_updated_at": null,
                "trans_code": "ZJTC50001",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "4,5",
                    "system_code": "system_type_001",
                    "id": 9,
                    "entry_model_code": "model_code_0009",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "0",
                                "perm_type": 0,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "报警主机主电故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "4",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "报警主机主电故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "5",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "主电",
                                "selected": "1",
                                "identifier": "prop_main_power",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "报警主机备电故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "6",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "报警主机备电故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "7",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "备电",
                                "selected": "1",
                                "identifier": "prop_emergency_power",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "4",
                                            "eventCode": "MUTE",
                                            "eventText": "消音",
                                            "id": 2003,
                                            "name": "消音",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "15",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "蜂鸣开关",
                                "selected": "1",
                                "identifier": "prop_silence",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "1",
                                            "eventCode": "FAULT",
                                            "eventText": "故障",
                                            "id": 2000,
                                            "name": "回路故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "8",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "-1"
                                        },
                                        {
                                            "type": "2",
                                            "eventCode": "FAULT_REALSE",
                                            "eventText": "故障解除",
                                            "id": 2001,
                                            "name": "回路故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "9",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "-1"
                                        },
                                        {
                                            "type": "offline",
                                            "eventCode": "OFFLINE",
                                            "eventText": "离线",
                                            "id": 2013,
                                            "name": "用传串口故障",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "149",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "recovery",
                                            "eventCode": "OFFLINE_REALSE",
                                            "eventText": "离线解除",
                                            "id": 2014,
                                            "name": "用传串口故障解除",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "150",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "回路",
                                "selected": "1",
                                "identifier": "prop_loop",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "recovery",
                                            "eventCode": "RESET",
                                            "eventText": "复位",
                                            "id": 2002,
                                            "name": "主机复位",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "14",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "-1"
                                        }
                                    ]
                                },
                                "name": "复位",
                                "selected": "1",
                                "identifier": "host_reset",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "type": "LOCAL",
                                "selected": "1",
                                "overwrite": "1",
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "isNormalType": "",
                                            "id": 2040,
                                            "eventText": "巡检正常",
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "identifier": "P0001",
                                "name": "巡检",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S31_02) && (S32_02) && (S33_02) && (S34_02) && (S34_04)",
                                "resultDesc": "",
                                "condition_desc": "主电(主电正常) && 备电(备电正常) && 消音(无消音) && 回路(故障解除) && 回路(离线恢复)",
                                "resultId": [
                                    null
                                ],
                                "result": null
                            },
                            {
                                "condition": "(S32_01) || (S31_01)",
                                "resultDesc": "",
                                "condition_desc": "备电(备电故障) || 主电(主电故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01",
                                    "S03_002_01_01"
                                ],
                                "result": "S03_002_01_01"
                            },
                            {
                                "condition": "(S34_01)",
                                "resultDesc": "",
                                "condition_desc": "回路(故障)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_01",
                                    "S03_002_01_02"
                                ],
                                "result": "S03_002_01_02"
                            },
                            {
                                "condition": "(S34_03)",
                                "resultDesc": "",
                                "condition_desc": "回路(离线)",
                                "resultId": [
                                    "S03_001",
                                    "S03_001_01"
                                ],
                                "result": "S03_001_01"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_07"
                                ],
                                "result": "S03_002_07"
                            },
                            {
                                "condition": "(S33_01)",
                                "resultDesc": "",
                                "condition_desc": "消音(消音)",
                                "resultId": [
                                    "S03_005",
                                    "S03_005_06"
                                ],
                                "result": "S03_005_06"
                            }
                        ]
                    },
                    "icon_level": 0,
                    "is_edit": 0,
                    "parent_type_code": "device_type_001_001_001_001",
                    "image_files": [

                    ],
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "主电",
                                "_index": 0,
                                "identifier": "state_main_power",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "主电故障",
                                                "typeCode": "5",
                                                "code": "S31_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_main_power",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "主电正常",
                                                "typeCode": "15",
                                                "code": "S31_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_main_power",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 199
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "备电",
                                "_index": 1,
                                "identifier": "state_emergency_power",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "备电故障",
                                                "typeCode": "5",
                                                "code": "S32_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_emergency_power",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "备电正常",
                                                "typeCode": "15",
                                                "code": "S32_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_emergency_power",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 213
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "消音",
                                "_index": 2,
                                "identifier": "state_silence",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "消音",
                                                "typeCode": "13",
                                                "code": "S33_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2003,
                                                    "propId": "prop_silence",
                                                    "eventCode": "MUTE",
                                                    "eventText": "消音"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "无消音",
                                                "typeCode": "15",
                                                "code": "S33_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 241
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "回路",
                                "_index": 3,
                                "identifier": "state_loop",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "故障",
                                                "code": "S34_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2000,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT",
                                                    "eventText": "故障"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "故障解除",
                                                "code": "S34_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2001,
                                                    "propId": "prop_loop",
                                                    "eventCode": "FAULT_REALSE",
                                                    "eventText": "故障解除"
                                                },
                                                {
                                                    "eventId": 2002,
                                                    "propId": "host_reset",
                                                    "eventCode": "RESET",
                                                    "eventText": "复位"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "离线",
                                                "typeCode": "",
                                                "code": "S34_03"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2013,
                                                    "propId": "prop_loop",
                                                    "eventCode": "OFFLINE",
                                                    "eventText": "离线"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "离线恢复",
                                                "typeCode": "",
                                                "code": "S34_04"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2014,
                                                    "propId": "prop_loop",
                                                    "eventCode": "OFFLINE_REALSE",
                                                    "eventText": "离线解除"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 255
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "巡检状态",
                                "_index": 4,
                                "identifier": "patrol_state",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S35_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S35_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 260
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S99_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 5,
                    "device_type_name": "联动型火灾报警控制器",
                    "is_end": 1,
                    "icon_prefix": "device_type",
                    "device_type_code": "device_type_001_001_001_001_005"
                },
                "extra_info": {

                },
                "parent_channel": "1",
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe05884e138235129b83c0f_device",
                "scrap_date": null,
                "vr_scene_id": "scene_78ab4b23aa680686",
                "product_date": null,
                "device_model": "ZJ-TC5000",
                "mark_range": {
                    "x": "504.6296",
                    "w": "1000.0",
                    "y": "446.6395",
                    "h": "1000.0",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": "hs-2070834273",
                "data_updated_at": "2020-12-21 16:10:44",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_001_001_001_001_005",
                "latitude": "31.870149",
                "is_delete": 0,
                "longtitude": "117.278411",
                "installation_date": "2020-12-21 00:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-25 15:59:18",
                "model_id": "",
                "device_name": "联动型火灾报警控制器",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":1,\"x\":\"\",\"y\":\"\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "",
                "is_indoor": 1
            }
        },
        {
            "center": {
                "x": 459.2648296,
                "y": 55.849916320000005
            },
            "bgColor": "blue",
            "icon": "L_device_type_002_001_001_001",
            "w": "1000.0",
            "h": "1000.0",
            "ext": {
                "vr_view_uuid": "980e25ac0e8f138f",
                "status": null,
                "address": "老区101大楼F7",
                "coordinate_address": "安徽省合肥市蜀山区38所国防通道碧林园西北91米",
                "space_id": "5fe03920e1382303852ca91d_space_floor",
                "created_at": "2020-12-21 14:42:24",
                "offline_updated_at": null,
                "trans_code": "",
                "space_path": "5fdc67b8e1382368b51b1dc2_space_factory,5fdc6c8ce1382368b517ec82_space_building,5fe03920e1382303852ca91d_space_floor",
                "relate_id": "",
                "device_code": "",
                "has_monitor": 0,
                "is_expire": 0,
                "deviceTypeInfo": {
                    "device_type_tag": "2,4,5",
                    "system_code": "system_type_008",
                    "id": 592,
                    "entry_model_code": "model_code_0008",
                    "profile": {
                        "properties": [
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "absense",
                                            "eventCode": "ABSENSE",
                                            "eventText": "人员离岗",
                                            "id": 2035,
                                            "name": "人员离岗报警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0011",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "work",
                                            "eventCode": "WORKING",
                                            "eventText": "工作进行",
                                            "id": 2033,
                                            "name": "离岗恢复",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0012",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "离岗检测",
                                "selected": "1",
                                "identifier": "prop_absence",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "1",
                                "perm_type": 1,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "alert",
                                            "eventCode": "ALERT",
                                            "eventText": "火灾报警",
                                            "id": 2007,
                                            "name": "烟火识别报警",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0009",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "recovery",
                                            "eventCode": "ALERT_REALSE",
                                            "isNormalType": "",
                                            "id": 2008,
                                            "eventText": "火灾报警解除",
                                            "name": "烟火识别报警恢复",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0010",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "烟火识别报警",
                                "selected": "1",
                                "identifier": "prop_smoke_alarm",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 0,
                                "source": "LOCAL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "dataabnor",
                                            "eventCode": "ABNORMAL",
                                            "eventText": "异常",
                                            "id": 2011,
                                            "name": "消防通道占用",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0015",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "recovery",
                                            "eventCode": "ABNORMAL_REALSE",
                                            "isNormalType": "",
                                            "id": 2012,
                                            "eventText": "无异常",
                                            "name": "消防通道未占用",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0016",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "通道占用",
                                "selected": "1",
                                "identifier": "prop_access_occupy",
                                "type": "LOCAL",
                                "monitorType": 1
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 0,
                                "source": "PATROL",
                                "dataType": {
                                    "needFix": "",
                                    "valueRange": "",
                                    "dataRule": {
                                        "defaultRuleValue": "L=undefined+undefined*D"
                                    },
                                    "type": "enum",
                                    "specs": {
                                        "step": ""
                                    },
                                    "eventRule": [
                                        {
                                            "type": "patrolabnor",
                                            "eventCode": "PATROL_ABNOR",
                                            "eventText": "巡检异常",
                                            "id": 2034,
                                            "name": "巡检异常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0001",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        },
                                        {
                                            "type": "other",
                                            "eventCode": "PATROL_NOR",
                                            "isNormalType": "",
                                            "id": 2040,
                                            "eventText": "巡检正常",
                                            "name": "巡检正常",
                                            "triggerConditions": [
                                                {
                                                    "expressions": [
                                                        {
                                                            "value": "P0003",
                                                            "condition": "eq"
                                                        }
                                                    ]
                                                }
                                            ],
                                            "effectScope": "0"
                                        }
                                    ]
                                },
                                "name": "巡检",
                                "selected": "1",
                                "identifier": "P0001",
                                "type": "LOCAL",
                                "monitorType": 1
                            }
                        ]
                    },
                    "status_rule": {
                        "rule": [
                            {
                                "condition": "(S01_01)",
                                "resultDesc": "",
                                "condition_desc": "离岗检测(人员离岗)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_08"
                                ],
                                "result": "S03_002_08"
                            },
                            {
                                "condition": "(S01_02)",
                                "resultDesc": "",
                                "condition_desc": "离岗检测(离岗恢复)",
                                "resultId": [
                                    null
                                ],
                                "result": null
                            },
                            {
                                "condition": "(S02_01)",
                                "resultDesc": "",
                                "condition_desc": "烟火识别(烟火识别报警)",
                                "resultId": [
                                    "S03_003",
                                    "S03_003_07"
                                ],
                                "result": "S03_003_07"
                            },
                            {
                                "condition": "(S99_01)",
                                "resultDesc": "",
                                "condition_desc": "巡检状态(巡检异常)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_07"
                                ],
                                "result": "S03_002_07"
                            },
                            {
                                "condition": "(S031_01)",
                                "resultDesc": "",
                                "condition_desc": "通道占用(通道占用)",
                                "resultId": [
                                    "S03_002",
                                    "S03_002_09"
                                ],
                                "result": "S03_002_09"
                            }
                        ]
                    },
                    "icon_level": 4,
                    "is_edit": 0,
                    "parent_type_code": "device_type_002_001_001_001",
                    "image_files": [

                    ],
                    "status_config": {
                        "status": [
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "人员离岗",
                                                "typeCode": "10",
                                                "code": "S01_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2035,
                                                    "propId": "prop_absence",
                                                    "eventCode": "ABSENSE",
                                                    "eventText": "人员离岗"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "离岗恢复",
                                                "typeCode": "10",
                                                "code": "S01_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2033,
                                                    "propId": "prop_absence",
                                                    "eventCode": "WORKING",
                                                    "eventText": "工作进行"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "离岗检测",
                                "identifier": "state_absence"
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "烟火识别报警",
                                                "typeCode": "6",
                                                "code": "S02_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2007,
                                                    "propId": "prop_smoke_alarm",
                                                    "eventCode": "ALERT",
                                                    "eventText": "火灾报警"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "烟火识别无报警",
                                                "typeCode": "15",
                                                "code": "S02_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2008,
                                                    "propId": "prop_smoke_alarm",
                                                    "eventCode": "ALERT_REALSE",
                                                    "eventText": "火灾报警解除"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "烟火识别",
                                "identifier": "state_smoke_alarm"
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "通道占用",
                                                "typeCode": "16",
                                                "code": "S031_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2011,
                                                    "propId": "prop_access_occupy",
                                                    "eventCode": "ABNORMAL",
                                                    "eventText": "异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "通道无占用",
                                                "typeCode": "15",
                                                "code": "S03_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2012,
                                                    "propId": "prop_access_occupy",
                                                    "eventCode": "ABNORMAL_REALSE",
                                                    "eventText": "无异常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "通道占用",
                                "identifier": "state_access_occupy"
                            },
                            {
                                "overwrite": "0",
                                "perm_type": 1,
                                "name": "巡检状态",
                                "_index": 3,
                                "identifier": "patrol_state",
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S04_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S04_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "_rowKey": 106
                            },
                            {
                                "dataType": {
                                    "config": [
                                        {
                                            "status": {
                                                "desc": "巡检异常",
                                                "typeCode": "16",
                                                "code": "S99_01"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2034,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_ABNOR",
                                                    "eventText": "巡检异常"
                                                }
                                            ]
                                        },
                                        {
                                            "status": {
                                                "desc": "巡检正常",
                                                "typeCode": "15",
                                                "code": "S99_02"
                                            },
                                            "source": [
                                                {
                                                    "eventId": 2040,
                                                    "propId": "P0001",
                                                    "eventCode": "PATROL_NOR",
                                                    "eventText": "巡检正常"
                                                }
                                            ]
                                        }
                                    ],
                                    "type": "enum"
                                },
                                "name": "巡检状态",
                                "overwrite": "0",
                                "identifier": "state_patrol"
                            }
                        ]
                    },
                    "remark": "",
                    "is_public": 0,
                    "device_type_level": 5,
                    "device_type_name": "可见光半球形摄像机",
                    "is_end": 1,
                    "icon_prefix": "device_type_002_001_001_001",
                    "device_type_code": "device_type_002_001_001_001_002"
                },
                "extra_info": {
                    "video_info": {
                        "access_way_code": "video_mode_002",
                        "is_fire_control": "0",
                        "camera_brand_code": "camera_brand_001"
                    }
                },
                "parent_channel": null,
                "space_type": 3,
                "is_fire_station_device": 0,
                "id": "5fe04382e13823180c27130c_device",
                "scrap_date": null,
                "vr_scene_id": "scene_78ab4b23aa680686",
                "product_date": null,
                "device_model": "device_model_002_001_001_001_002",
                "mark_range": {
                    "x": "920.3704",
                    "w": "1000.0",
                    "y": "111.92368",
                    "h": "1000.0",
                    "draw_id": "5fe04319e13823180a347257_res_drawing",
                    "draw_address": "老区101大楼F7"
                },
                "unit_id": "5fd6cffae138236218375ede_social_unit",
                "vr_id": "hs-2079814321",
                "data_updated_at": "2020-12-21 14:41:06",
                "is_public": 0,
                "region_code": "340102001001",
                "device_type": "device_type_002_001_001_001_002",
                "latitude": "31.859538458133947",
                "is_delete": 0,
                "longtitude": "117.24749649570936",
                "installation_date": "2020-12-21 08:00:00",
                "model_type": "",
                "parent_id": "",
                "updated_at": "2020-12-23 15:28:12",
                "model_id": "",
                "device_name": "f7可见光半球形摄像机",
                "model_name": "",
                "manufactor_id": "",
                "model_position": "",
                "fmap_position": "{\"map_id\":\"1320981210342641666\",\"space_name\":\"F7\",\"x\":\"1.3052562824902344E7\",\"y\":\"3745706.811910156\"}",
                "sim_expire_time": null,
                "sim_card_number": "",
                "device_brand": "camera_brand_001",
                "is_indoor": 1
            }
        }
    ],
    "ranges": [

    ]
})
