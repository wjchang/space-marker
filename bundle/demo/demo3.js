let el = document.querySelector('.map')

let canvas = new BwwlSpaceMarker.SpaceMarker(el, {
    mapImgUrl: 'http://web.cdn.cetcsafe.com/banner.png',
    on: (event, data) => {
        console.log(event, data)
    }
})

let re_init = document.getElementById('re_init')
re_init.onclick = () => {
    canvas.reset({
        editModel:'mark',
        editOption: {
            bgColor: 'red',
        },
        mapImgUrl: 'http://bwwl-platform.oss-cn-shanghai.aliyuncs.com/ali_oss/5e4b4e5de138234d4d2c3cd0_res_file.svg',
        marks: [{
            'center': { x: "64.66665649414062",y: "92.99703087885985" },
            'bgColor': '#F09614',
            'icon': 'L_device_type_001_001_002_009',
            h: "264.9940617577197",
            w: "375.0",
            'ext': {
                'status': ['S03_001_01'],
                'address': '蜀山区工业园test',
                'coordinate_address': '安徽省合肥市庐阳区三孝口街道中国电科十六所小区',
                'space_id': '5f06891be138231a391bbc3a_space_zone',
                'created_at': '2020-07-09 16:53:00',
                'offline_updated_at': null,
                'trans_code': 'lll0022',
                'space_path': '5e4b4e69e138234d4ab2ea99_space_factory,5f06891be138231a391bbc3a_space_zone',
                'has_monitor': 1,
                'device_type': 'device_type_001_001_002_009_004',
                'deviceTypeInfo': {
                    'device_type_tag': '1,5,6,8',
                    'system_code': 'system_type_011',
                    'id': 60,
                    'entry_model_code': 'model_code_0004',
                    'profile': {
                        'properties': [{
                            'monitorType': 0,
                            'overwrite': '1',
                            'identifier': '5675675675',
                            'source': 'LOCAL',
                            'dataType': {
                                'eventRule': [],
                                'needFix': '1',
                                'specs': { 'value': '14', 'unit': 'kg/m3', 'step': '5675', 'unitName': '千克每立方米' },
                                'dataRule': { 'defaultBValue': '57567' },
                                'valueRange': '56756',
                                'conditionData': {
                                    'D_HH': '7',
                                    'STANDARD': '7',
                                    'D_H_R': '',
                                    'CHANGE_LIMIT': '',
                                    'D_HH_R': '',
                                    'D_LL_R': '',
                                    'D_H': '',
                                    'D_L_R': '',
                                    'D_LL': '',
                                    'D_L': ''
                                },
                                'required': ['STANDARD', 'D_HH'],
                                'type': 'double'
                            },
                            'name': '567567',
                            'type': 'LOCAL',
                            'channel': '0'
                        }, {
                            'overwrite': '1',
                            'monitorType': 1,
                            'source': 'RELATE',
                            'dataType': {
                                'needFix': '',
                                'specs': { 'step': '' },
                                'dataRule': {},
                                'valueRange': '',
                                'eventRule': [{
                                    'eventText': '',
                                    'triggerConditions': [{ 'expressions': [{ 'value': '', 'condition': 'eq' }] }],
                                    'name': '',
                                    'eventCode': '',
                                    'type': ''
                                }],
                                'type': 'enum'
                            },
                            'name': '烟雾状态',
                            'identifier': '2',
                            'type': 'RELATE'
                        }]
                    },
                    'status_rule': null,
                    'icon_level': 4,
                    'parent_type_code': 'device_type_001_001_002_009',
                    'image_files': [],
                    'device_type_code': 'device_type_001_001_002_009_004',
                    'icon_prefix': 'device_type_001_001_002_009',
                    'is_public': 0,
                    'status_config': null,
                    'device_type_name': '独立式复合感烟感温火灾探测报警器',
                    'is_end': 1,
                    'device_type_level': 5,
                    'remark': ''
                },
                'extra_info': {},
                'parent_channel': null,
                'id': '5f06daf3e138231a39100137_device',
                'device_brand': '',
                'product_date': null,
                'device_model': 'SCU200-TND',
                'mark_range': {
                    'x': '343',
                    'w': '1000',
                    'y': '361',
                    'h': '707',
                    'draw_address': '蜀山区工业园test',
                    'draw_id': '5e4b4e69e138234d4ab51878_res_drawing'
                },
                'unit_id': '5e4b4e2be138234d4a2971e2_social_unit',
                'parent_id': '',
                'data_updated_at': '2020-07-09 16:53:07',
                'region_code': '340104000000',
                'model_position': '',
                'latitude': '31.866001',
                'is_delete': 0,
                'model_name': '',
                'model_type': '',
                'device_name': '独立式复合感烟感温火灾探测报警器',
                'model_id': '',
                'updated_at': null,
                'is_indoor': 0,
                'installation_date': null,
                'longtitude': '117.276176',
                'manufactor_id': '',
                'is_public': 0,
                'scrap_date': null,
                'is_fire_station_device': 0,
                'space_type': 7,
                'device_code': 'dada',
                'relate_id': ''
            }
        }, {
            'center': { 'x': '468', 'y': '128' },
            'bgColor': 'blue',
            'icon': 'L_device_type_002_001_001_001',
            'w': '1000',
            'h': '707',
            'ext': {
                'status': null,
                'address': '大大猜猜猜',
                'coordinate_address': '四川省成都市成华区龙潭街道北三环路四段',
                'space_id': '5f06891be138231a391bbc3a_space_zone',
                'created_at': '2020-07-09 16:50:53',
                'offline_updated_at': null,
                'trans_code': '',
                'space_path': '5e4b4e69e138234d4ab2ea99_space_factory,5f06891be138231a391bbc3a_space_zone',
                'has_monitor': 0,
                'device_type': 'device_type_002_001_001_001_001',
                'deviceTypeInfo': {
                    'device_type_tag': '2,4,5',
                    'system_code': 'system_type_008',
                    'id': 591,
                    'entry_model_code': 'model_code_0008',
                    'profile': null,
                    'status_rule': null,
                    'icon_level': 4,
                    'parent_type_code': 'device_type_002_001_001_001',
                    'image_files': null,
                    'device_type_code': 'device_type_002_001_001_001_001',
                    'icon_prefix': 'device_type_002_001_001_001',
                    'is_public': 0,
                    'status_config': null,
                    'device_type_name': '可见光球形摄像机',
                    'is_end': 1,
                    'device_type_level': 5,
                    'remark': ''
                },
                'extra_info': {
                    'video_info': {
                        'stream_url': 'da da',
                        'access_way_code': 'video_mode_001',
                        'is_fire_control': 0
                    }
                },
                'parent_channel': null,
                'id': '5f06da75e138232955378498_device',
                'device_brand': '',
                'product_date': null,
                'device_model': 'XH-001',
                'mark_range': {
                    'x': '468',
                    'w': '1000',
                    'y': '128',
                    'h': '707',
                    'draw_id': '5e4b4e69e138234d4ab51878_res_drawing'
                },
                'unit_id': '5e4b4e2be138234d4a2971e2_social_unit',
                'parent_id': '',
                'data_updated_at': '2020-07-09 16:51:01',
                'region_code': '340104000000',
                'model_position': '',
                'latitude': '30.711426',
                'is_delete': 0,
                'model_name': '',
                'model_type': '',
                'device_name': '柳叶刀2',
                'model_id': '',
                'updated_at': null,
                'is_indoor': 0,
                'installation_date': null,
                'longtitude': '104.137146',
                'manufactor_id': '',
                'is_public': 0,
                'scrap_date': null,
                'is_fire_station_device': 0,
                'space_type': 7,
                'device_code': '',
                'relate_id': ''
            }
        }, {
            'center': { 'x': '283', 'y': '296' },
            'bgColor': 'blue',
            'icon': 'L_device_type_002_001_001_001',
            'w': '1000',
            'h': '707',
            'ext': {
                'status': null,
                'address': '大大大',
                'coordinate_address': '四川省成都市青羊区文家街道土龙路',
                'space_id': '5f06891be138231a391bbc3a_space_zone',
                'created_at': '2020-07-09 16:50:13',
                'offline_updated_at': null,
                'trans_code': '',
                'space_path': '5e4b4e69e138234d4ab2ea99_space_factory,5f06891be138231a391bbc3a_space_zone',
                'has_monitor': 0,
                'device_type': 'device_type_002_001_001_001_001',
                'deviceTypeInfo': {
                    'device_type_tag': '2,4,5',
                    'system_code': 'system_type_008',
                    'id': 591,
                    'entry_model_code': 'model_code_0008',
                    'profile': null,
                    'status_rule': null,
                    'icon_level': 4,
                    'parent_type_code': 'device_type_002_001_001_001',
                    'image_files': null,
                    'device_type_code': 'device_type_002_001_001_001_001',
                    'icon_prefix': 'device_type_002_001_001_001',
                    'is_public': 0,
                    'status_config': null,
                    'device_type_name': '可见光球形摄像机',
                    'is_end': 1,
                    'device_type_level': 5,
                    'remark': ''
                },
                'extra_info': {
                    'video_info': {
                        'stream_url': '大大',
                        'access_way_code': 'video_mode_001',
                        'is_fire_control': 0
                    }
                },
                'parent_channel': null,
                'id': '5f06da4de138231a392ea511_device',
                'device_brand': '',
                'product_date': null,
                'device_model': 'XH-001',
                'mark_range': {
                    'x': '283',
                    'w': '1000',
                    'y': '296',
                    'h': '707',
                    'draw_id': '5e4b4e69e138234d4ab51878_res_drawing'
                },
                'unit_id': '5e4b4e2be138234d4a2971e2_social_unit',
                'parent_id': '',
                'data_updated_at': '2020-07-09 16:50:21',
                'region_code': '340104000000',
                'model_position': '',
                'latitude': '30.712607',
                'is_delete': 0,
                'model_name': '',
                'model_type': '',
                'device_name': '柳叶刀',
                'model_id': '',
                'updated_at': null,
                'is_indoor': 0,
                'installation_date': null,
                'longtitude': '103.940766',
                'manufactor_id': '',
                'is_public': 0,
                'scrap_date': null,
                'is_fire_station_device': 0,
                'space_type': 7,
                'device_code': '',
                'relate_id': ''
            }
        }],
        ranges: [{
            'points': [[509.41, 123.774], [233.234, 367.112], [259.336, 398.266], [530.46, 143.982], [530.46, 143.982]],
            'w': 1000,
            'h': 707,
            'ext': { 'space_id': '5f06891be138231a391bbc3a_space_zone', 'space_type': 7, 'name': 'test' }
        }, {
            'points': [[299.752, 97.672], [371.322, 197.028], [491.728, 102.724], [403.318, 5.052], [403.318, 5.052]],
            'w': 1000,
            'h': 600,
            'ext': { 'space_id': '5e4b4e95e138234d4aab72ab_space_zone', 'space_type': 7, 'name': '2号区域' }
        }, {
            'points': [[170.084, 205.448], [261.02, 308.172], [372.164, 212.184], [277.86, 115.354], [278.702, 115.354]],
            'w': 1000,
            'h': 600,
            'ext': { 'space_id': '5e4b4e83e138234d4ddc1941_space_zone', 'space_type': 7, 'name': '1号区域' }
        }, {
            'points': [[331.748, 256.81], [416.79, 351.956], [526.25, 287.964], [541.406, 242.496], [533.828, 206.29], [529.618, 203.764], [529.618, 203.764]],
            'w': 1000,
            'h': 707,
            'ext': { 'space_id': '5f07cd73e1382324001ee799_space_building', 'space_type': 6, 'name': 'test建筑' }
        }],
        on: (event, data) => {
            console.log(event, data)
        }



    })
}


let re_init2 = document.getElementById('re_init2')
re_init2.onclick = () => {
    canvas.reset({
        // resizeMode: 'coverRect', // coverRect,fitRectHeight
        mapImgUrl: 'http://bwwl-platform.oss-cn-shanghai.aliyuncs.com/ali_oss/5f093425e138235840119745_res_file.png',
        marks: [{
            'center': { 'x': '175', 'y': '293' },
            'bgColor': '#F09614',
            'icon': 'L_device_type_001_001_002_009',
            'w': '1000',
            'h': '707',
            // fit: true,
            ext: {
                index: '2'
            }
        },
            {
                'center': { 'x': '275', 'y': '293' },
                'bgColor': '#F09614',
                'icon': 'L_device_type_001_001_002_009',
                'w': '1000',
                'h': '707',
                // fit: true,
                ext: {
                    index: '3'
                }
            }
        ],
    })
}

let re_init3 = document.getElementById('re_init3')
re_init3.onclick = () => {
    canvas.reset({
        mapImgUrl: 'http://web.cdn.cetcsafe.com/banner.png',
        on: (event, data) => {
            console.log(event, data)
        }
    })
}

let re_update_mark = document.getElementById('re_update_mark')
re_update_mark.onclick = () => {
    canvas.updateMark('2', {
        bgColor: (['blue', 'red', 'orange', 'yellow'])[Math.round(Math.random() * 10) % 4],
        icon: (['L_device_type_005_001_001', 'device_abnormal', 'device_normal', 'device_offline', 'device_alert', 'device_video', 'position', 'L_device_type_003_001_001', 'L_device_type_004_004_006'])[Math.round(Math.random() * 10) % 9],
    }, 'ext.index')
}

// canvas.loadMapImg('http://bwwl-platform-cetc.oss-cn-hangzhou.aliyuncs.com/ali_oss/5eaa6a91adca681c111e5314_res_file.png')
