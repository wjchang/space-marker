import {Pen} from "./pen";
import {Point} from "../model/point";
import rangeOptions from "../const/default/rangeOptions";
import {SpaceMarker} from "../core";
import * as _ from '../util/lodash'
import * as Color from 'color'
import {RangeBgType} from '../const/rangeType'
import {Rect} from "../model/rect";

export class Range extends Pen {
    private bgColor;
    public isActive: boolean = false;

    public ext = {};
    public options = {};
    public points = [];
    public borderRect: Rect;
    public visible: boolean = true;
    public spaceMarker: SpaceMarker = null;

    constructor(options = {}, spaceMarker: SpaceMarker) {
        super(_.get(options, 'id'));
        this.spaceMarker = spaceMarker;

        let _options = Object.assign({}, rangeOptions, options);

        // 针对企业图纸缩放算法坐标进行兼容
        _options.points = _options.points.map(m => {
            if (_.get(_options, 'w')) {
                let _h = _.get(_options,'h') || _.get(_options,'w');
                m[0] = (m[0] * this.spaceMarker.mapImg.width) / _.get(_options, 'w');
                m[1] = (m[1] * this.spaceMarker.mapImg.height) / _h;
            }

            return m;
        })

        this.options = _options;

        this.parseOptions();
    }

    parseOptions() {
        const options = this.options;
        this.bgColor = _.get(options, 'bgColor');
        this.ext = _.get(options, 'ext');
        this.visible = _.get(options, 'visible')

        if (RangeBgType[this.bgColor]) {
            this.bgColor = RangeBgType[this.bgColor]
        }

        this.updatePoints()
    }

    private updatePoints() {
        let points = _.get(this.options, 'points') || [];

        this.points = points.map(m => {
            let cx = (m[0] - this.spaceMarker.mapImgRect.x) * this.spaceMarker.ratio;
            let cy = (m[1] - this.spaceMarker.mapImgRect.y) * this.spaceMarker.ratio;
            return [cx, cy]
        })

        this.calcBorderRect();
    }

    private calcBorderRect() {
        let xs = this.points.map(m => m[0]);
        let ys = this.points.map(m => m[1]);

        let minX = _.min(xs);
        let maxX = _.max(xs);
        let minY = _.min(ys);
        let maxY = _.max(ys);

        this.borderRect = new Rect(minX, minY, maxX - minX, maxY - minY)
    }

    /**
     * 射线法判断点是否在多边形内部
     * https://blog.csdn.net/u283056051/article/details/53980832?utm_source=blogxgwz7
     * @param pos
     */
    hit(pos: Point) {
        let px = pos.x, py = pos.y, flag = false;
        let poly = this.points.map(m => new Point(m[0], m[1]))

        for (let i = 0, l = poly.length, j = l - 1; i < l; j = i, i++) {
            let sx = poly[i].x,
                sy = poly[i].y,
                tx = poly[j].x,
                ty = poly[j].y

            // 点与多边形顶点重合
            if ((sx === px && sy === py) || (tx === px && ty === py)) {
                return true
            }

            // 判断线段两端点是否在射线两侧
            if ((sy < py && ty >= py) || (sy >= py && ty < py)) {
                // 线段上与射线 Y 坐标相同的点的 X 坐标
                let x = sx + (py - sy) * (tx - sx) / (ty - sy)

                // 点在多边形的边上
                if (x === px) {
                    return true
                }

                // 射线穿过多边形的边界
                if (x > px) {
                    flag = !flag
                }
            }
        }

        // 射线穿过多边形边界的次数为奇数时点在多边形内
        return flag
    }

    click(): void {
    }

    dblclick(): void {
    }

    public setActive(isActive: boolean) {
        this.isActive = isActive;
        this.spaceMarker.drawMapImage();
        return this;
    }

    public update(options) {
        if (_.get(options, 'bgColor')) {
            this.bgColor = _.get(options, 'bgColor');

            if (RangeBgType[this.bgColor]) {
                this.bgColor = RangeBgType[this.bgColor]
            }
        }

        if (!_.isEmpty(_.get(options, 'ext'))) {
            this.ext = _.get(options, 'ext');
        }

        if(_.get(options, 'visible') !== undefined) {
            this.visible = _.get(options, 'visible')
        }
    }

    draw(): void {
        if(!this.visible) return;

        if (this.points.length <= 0) return;

        this.updatePoints()

        let ctx = this.spaceMarker.ctx;
        ctx.save()

        ctx.beginPath();
        ctx.moveTo(this.points[0][0], this.points[0][1])

        for (let i = 1; i < this.points.length; i++) {
            ctx.lineTo(this.points[i][0], this.points[i][1])
        }
        ctx.closePath();

        ctx.fillStyle = this.bgColor;
        if (this.isActive) {
            // ctx.fillRect(this.borderRect.x,this.borderRect.y,this.borderRect.width,this.borderRect.height)
            ctx.strokeStyle = Color(this.bgColor).alpha(0.8).lighten(0.2);
            ctx.lineWidth = 2;
            ctx.stroke();
        }

        ctx.fill()
        ctx.restore();
    }
}
