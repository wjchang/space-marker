import {s8} from "../util/lodash";

export abstract class Pen {
    public id = '';
    public name = '';

    protected constructor(id) {
        if(!!id) {
            this.id = id;
        } else {
            this.id = s8()
        }
    }

    /**
     * 绘制
     */
    abstract draw(): void;

    /**
     * 点击
     */
    abstract click(): void;

    /**
     * 双击
     */
    abstract dblclick(): void;
}
