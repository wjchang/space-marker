import {Pen} from "./pen";
import {Point} from "../model/point";
import defaultMarkOptions from "../const/default/markOptions";
import {SpaceMarker} from "../core";
import * as _ from '../util/lodash'
import {Rect} from "../model/rect";
import resources from '../resource'
import * as Color from 'color'

import {MarkBgType} from '../const/markType'

export class Mark extends Pen {
    private center: Point;
    private radius: number = 0;
    private padding = 0;
    private icon;
    private iconImg;
    private iconFont;
    private bgColor;

    public ext = {};
    private borderRect: Rect;
    private centerRect: Rect;
    public isActive: boolean = false;
    public visible: boolean = true;
    public highlight: boolean = false;
    public highlightSize: number;

    public options = {};
    public spaceMarker: SpaceMarker = null;


    constructor(options = {}, spaceMarker: SpaceMarker) {
        super(_.get(options,'id'));
        this.spaceMarker = spaceMarker;
        let _options = Object.assign({}, defaultMarkOptions, options);

        // 针对企业图纸缩放算法坐标进行兼容
        if(_.get(_options,'w') ) {
            let _h = _.get(_options,'h') || _.get(_options,'w');
            _options.center.x = (_options.center.x * this.spaceMarker.mapImg.width) / _.get(_options,'w');
            _options.center.y = (_options.center.y * this.spaceMarker.mapImg.height) / _h;
        }

        this.options = _options

        this.parseOptions();
    }

    parseOptions() {
        const options = this.options;

        this.calcCenter();

        this.radius = _.get(options, 'radius')
        this.padding = _.get(options, 'padding')
        this.icon = _.get(options, 'icon')
        this.bgColor = _.get(options, 'bgColor')
        this.highlight = _.get(options, 'highlight')
        this.visible = _.get(options, 'visible')
        this.highlightSize = _.get(options, 'highlightSize')
        this.ext = _.get(options,'ext') || {};

        this.parseIcon()

        if (MarkBgType[this.bgColor]) {
            this.bgColor = MarkBgType[this.bgColor]
        }

        if(_.get(options,'fit')) {
            this.spaceMarker.center(this.center);
        }
    }

    private parseIcon() {
        if(this.icon.startsWith('http')) {
            this.iconFont = null;
            this.loadImg(this.icon)
        } else if (resources[`${this.icon}`]) {
            this.iconFont = null;
            this.loadImg(resources[`${this.icon}`])
        } else if (_.parseIcon(this.icon)) {
            this.iconImg = null;
            this.iconFont = _.parseIcon(this.icon)
        } else {
            this.iconImg = null;
            this.icon = 'L_device_type'
            this.iconFont = _.parseIcon('L_device_type')
        }
    }

    public calcCenter() {
        let cx = (_.get(this.options, 'center.x') - this.spaceMarker.mapImgRect.x) * this.spaceMarker.ratio;
        let cy = (_.get(this.options, 'center.y') - this.spaceMarker.mapImgRect.y) * this.spaceMarker.ratio;
        this.center = new Point(cx, cy);

        let innerWidth = this.radius - this.padding;
        this.centerRect = new Rect(this.center.x - innerWidth, this.center.y - innerWidth, innerWidth * 2, innerWidth * 2);
        this.borderRect = new Rect(this.center.x - this.radius, this.center.y - this.radius, this.radius * 2, this.radius * 2);
    }

    public loadImg(src, cb?: any) {
        if (!src) {
            return;
        }

        this.iconImg = new Image();
        this.iconImg.crossOrigin = 'anonymous';
        this.iconImg.src = src;
        this.iconImg.onload = () => {
            this.draw();
            if (cb) {cb();}
        };
    }

    hit(pos: Point) {
        return Math.sqrt(Math.pow(pos.x - this.center.x, 2) + Math.pow(pos.y - this.center.y, 2)) <= this.radius;
    }

    click(): void {
    }

    dblclick(): void {
    }

    public setActive(isActive: boolean) {
        this.isActive = isActive;
        this.spaceMarker.drawMapImage();
        return this;
    }

    public update(options) {
        this.options = Object.assign({}, this.options, options);
        this.parseOptions()
    }

    draw(flag ?: boolean): void {
        if(!this.visible) return;

        this.calcCenter();

        // 超出屏幕范围的不画出来
        if (this.center.x < 0 || this.center.y < 0 || this.center.x > this.spaceMarker.map.width || this.center.y > this.spaceMarker.map.height) {
            return;
        }

        let ctx = this.spaceMarker.ctx;
        ctx.save()

        if(this.highlight) {
            ctx.beginPath();
            ctx.arc(this.center.x, this.center.y, this.radius + this.highlightSize, 0, 2 * Math.PI, false)
            ctx.fillStyle = Color(this.bgColor).alpha(0.4).lighten(0.5);
            ctx.fill()

            ctx.strokeStyle = Color(this.bgColor).alpha(0.8).lighten(0.2);
            ctx.lineWidth = 1;
            ctx.stroke();
        }

        if (this.iconImg) {
            ctx.beginPath();
            ctx.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI, false)
            ctx.fillStyle = this.bgColor;
            ctx.fill()

            ctx.drawImage(this.iconImg, 0, 0, this.iconImg.width, this.iconImg.height, this.centerRect.x, this.centerRect.y, this.centerRect.width, this.centerRect.height)
        }

        if (this.iconFont) {
            // 因为字体图标是镂空的，所以先画一个白底
            ctx.beginPath();
            if(this.icon.startsWith('L_')) {
                ctx.arc(this.center.x, this.center.y, this.radius, 0, 2 * Math.PI, false)
                ctx.fillStyle = Color(this.bgColor).alpha(0.8);
            } else {
                ctx.arc(this.center.x, this.center.y, this.radius - 0.2, 0, 2 * Math.PI, false)
                ctx.fillStyle = 'rgba(255,255,255,1)';
            }

            ctx.fill()

            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';

            if(this.icon.startsWith('L_')) {
                ctx.fillStyle = 'rgba(255,255,255,1)';
                ctx.font = `${this.borderRect.width - this.padding / 2}px anticon`;
            } else {
                ctx.font = `${this.borderRect.width}px anticon`;
                ctx.fillStyle = this.bgColor;
            }

            ctx.fillText(this.iconFont, this.center.x, this.center.y);
        }

        if (this.isActive) {
            ctx.strokeStyle = Color(this.bgColor).alpha(0.8).lighten(0.2);
            ctx.lineWidth = 3;
            ctx.stroke();
        }

        ctx.restore();
    }
}
