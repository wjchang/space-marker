import {Point} from "../model/point";

export class Rect {
    public ex: Point = null;
    public center: Point = null;
    public topCenter: Point = null;

    constructor(public x: number, public y: number, public width: number, public height: number) {
        this.ex = new Point(x + width, y + height);
        this.center = new Point(x + width / 2, y + height / 2);
        this.topCenter = new Point(x + width / 2, y);
    }
}
