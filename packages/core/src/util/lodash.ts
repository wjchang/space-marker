import get from 'lodash/get';
import isEmpty from 'lodash/isEmpty';
import isArray from 'lodash/isArray';
import isBoolean from 'lodash/isBoolean';
import isString from 'lodash/isString';
import compact from 'lodash/compact';
import uniq from 'lodash/uniq';
import min from 'lodash/min';
import max from 'lodash/max';
import iconfont from "../const/iconfont";

function s8() {
    return (((1 + Math.random()) * 0x100000000) | 0).toString(16).substring(1);
}

function parseIcon(class_name) {
    let icon = iconfont.find(f=>f.class_name == class_name);
    return get(icon, 'val') || '';
}

function getFileExt(file, hasDot = false) {
    const xp = file.lastIndexOf('.')
    return hasDot ? file.substring(xp, file.length).toLowerCase() : file.substring(xp + 1, file.length).toLowerCase()
}

export {
    get,
    isEmpty,
    s8,
    min,
    max,
    isArray,
    uniq,
    compact,
    isBoolean,
    isString,
    parseIcon,
    getFileExt,
}
