export enum MarkBgType {
    blue = 'rgba(9, 140, 255, 0.8)',
    red = 'rgba(220, 40, 40, 0.8)',
    orange = 'rgba(255,170,40,0.8)',
    yellow = 'rgba(250, 230, 0, 0.8)',
}
