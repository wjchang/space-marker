export default {
    center: {x: 0, y: 0},
    ext: {},
    radius: 18,
    padding: 9,
    highlight: false,
    visible: true,
    highlightSize: 9,
    icon: 'L_device_type',  //device_abnormal,device_normal,device_offline,device_alert,device_video,position
    bgColor: 'blue',        //blue,red,orange,yellow
    fit: false,             // 自动居中
}
