export default {
    mapWidth:0,
    mapHeight: 0,
    scaleX: 1,
    scaleY: 1,
    resizeMode: 'coverCenter', // coverRect,fitRectHeight
    translateImgTo: 1000,
    editModel: null,
    editOption: null,
    mapImgUrl: '',
    on: (event:string, data:any)=>{},
    marks: [],
    ranges:[],
}
