export enum RangeBgType {
    blue = 'rgba(9, 140, 255, 0.4)',
    red = 'rgba(220, 40, 40, 0.4)',
    orange = 'rgba(255, 170, 40, 0.4)',
    yellow = 'rgba(250, 230, 0, 0.4)',
}
