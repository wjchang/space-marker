import {Point} from "./model/point";
import * as _ from './util/lodash'
import {Mark} from "./drawable/mark";
import {MouseMoveType} from "./const/mouseMoveType";
import coreOptions from "./const/default/coreOptions";
import {Range} from "./drawable/range";

export class SpaceMarker {
    public debug = false;
    private width;
    private height;
    public map; // canvas
    public ctx; // canvas.ctx
    public marks: Mark[] = [];
    public ranges: Range[] = [];

    public buffer;
    public bufferCtx;

    public activeMark: Mark;
    public activeRange: Range;
    public hoverMark: Mark;
    public hoverRange: Range;
    public translateImgTo: number;
    public translateImgToRatio: number;
    public mapImg: HTMLImageElement
    public mapImgRect: { x: number; y: number; width: number; height: number; } = {x: 0, y: 0, width: 0, height: 0}

    private isDragStart: boolean = false;
    private mouseMoveType = MouseMoveType.CLICK;
    private startDragPos: Point = {x: 0, y: 0};
    public readonly triggerMoveDistance = 3;
    public scaleX: number = 1;
    public scaleY: number = 1;
    public isSvg: boolean = false;

    public ratio = 1;
    public dpiRatio = 2;
    private time;
    private currentImg = '';
    private resizeMode;
    private resizeModeFns = {
        'coverRect': this.coverRect.bind(this),
        'coverCenter': this.coverCenter.bind(this),
    }

    // 编辑模式
    private editModel = null;

    constructor(public parentElem: HTMLElement, public options) {
        this.debug = _.get(options, 'debug');

        if (this.debug) {
            console.log('space-marker>>>>>>>', 'constructor', HTMLElement, options, options)
        }

        this.checkDpiRatio(); // 动态调整dpi
        this.initialize(options);
        this._winResize();
    }

    checkDpiRatio() {
        if (window.devicePixelRatio < 2) {
            this.dpiRatio = 2;
        } else {
            this.dpiRatio = window.devicePixelRatio
        }
    }

    parseOptions(options) {
        this.ctx.clearRect(0, 0, this.width, this.height)
        this.isSvg && this.bufferCtx.clearRect(0, 0, this.buffer.width, this.buffer.height);
        this.marks = [];
        this.ranges = [];

        this.mapImgRect = {
            x: 0,
            y: 0,
            width: _.get(options, 'mapWidth') || 0,
            height: _.get(options, 'mapHeight') || 0
        }
        this.loadMapImg(_.get(options, 'mapImgUrl'), () => {
            this.onImgLoad(options);
        })
    }

    private onImgLoad(options) {
        this.currentImg = _.get(options, 'mapImgUrl');
        let _markers = _.get(options, 'marks') || []
        if (!_.isEmpty(_markers)) {
            this.marks = _markers.map(_m => {
                return new Mark(_m, this)
            })
        }

        let _ranges = _.get(options, 'ranges') || []
        if (!_.isEmpty(_ranges)) {
            this.ranges = _ranges.map(_r => {
                return new Range(_r, this)
            })
        }

        // 编辑模式
        this.setEditable(_.get(options,'editModel'),_.get(options,'editOption'))

        this.drawMapImage();
        this.dispatch('loaded', this)
    }

    public loadMapImg(src, cb?: any) {
        if (!src) {
            this.mapImg = new Image();
            this.mapImg.crossOrigin = 'anonymous';
            return;
        }

        this.mapImg = new Image();
        this.mapImg.crossOrigin = 'anonymous';
        this.mapImg.src = src + "?_=" + _.s8();
        this.mapImg.onload = () => {
            this.isSvg =  _.getFileExt(this.mapImg.src).startsWith('svg')
            this.translateImgToRatio = this.translateImgTo / this.mapImg.width;

            if (this.isSvg) {
                this.buffer = this.createMap(this.translateImgTo * this.dpiRatio * 3, this.translateImgToRatio * this.mapImg.height * this.dpiRatio * 3);
                this.bufferCtx = this.buffer.getContext('2d');
                this.bufferCtx.drawImage(this.mapImg, 0, 0, this.buffer.width, this.buffer.height);

                this.mapImg = new Image();
                this.mapImg.crossOrigin = 'anonymous';
                this.mapImg.src = this.buffer.toDataURL('image/png');

                this.mapImg.onload = () => {
                    this.mapImgRect = this.resizeModeFns[this.resizeMode](this.width, this.height, this.mapImg.width, this.mapImg.height);
                    if (cb) {
                        cb()
                    }
                }
            } else {
                this.mapImgRect = this.resizeModeFns[this.resizeMode](this.width, this.height, this.mapImg.width, this.mapImg.height);
                if (cb) {
                    cb()
                }
            }
        };
    }

    public drawMapImage() {
        if (!this.mapImg || !this.mapImgRect || !this.ctx) {
            return;
        }

        this.ctx.clearRect(0, 0, this.width, this.height)

        this.ctx.drawImage(this.mapImg, this.mapImgRect.x, this.mapImgRect.y, this.mapImgRect.width,
            this.mapImgRect.height, 0, 0, this.width, this.height);

        this.ranges.forEach(r => {
            r.draw();
        })

        this.marks.forEach(m => {
            m.draw();
        })

        this.dispatch('render', this);
    }

    private initialize(options) {
        if (!this.parentElem) {
            throw new Error('A DOM element is required for SpaceMarker to initialize')
        }

        this.options = Object.assign({}, coreOptions, options)

        this.width = this.parentElem.clientWidth;
        this.height = this.parentElem.clientHeight;

        if (this.debug) {
            console.log('space-marker>>>>>>>', 'initialize', this, this.width, this.height)
        }

        this.resizeMode = _.get(this.options, 'resizeMode');

        if(this.map) this.map.remove();

        this.map = this.createMap(this.width, this.height, this.dpiRatio);
        this.ctx = this.map.getContext('2d')
        this.ctx.scale(this.dpiRatio, this.dpiRatio);

        this.parentElem.append(this.map);

        this.map.onmousemove = this.onMouseMove;
        this.map.onmousedown = this.onMouseDown;
        this.map.onmouseup = this.onMouseUp;
        this.map.ondblclick = this.onDblClick;
        this.map.onmouseleave = this.onMouseLeave;
        this.map.oncontextmenu = this.onContextMenu;

        this.map.onblur = this.onBlur;
        this.map.onwheel = this.onWheel;


        this.scaleX = _.get(this.options, 'scaleX');
        this.scaleY = _.get(this.options, 'scaleY');

        this.translateImgTo = _.get(this.options, 'translateImgTo');

        this.parseOptions(options)
    }

    createMap(width: number, height: number, dpiRatio = 1) {
        let map = document.createElement('canvas');
        map.width = width * dpiRatio;
        map.height = height * dpiRatio;
        map.style.width = width + 'px';
        map.style.height = height + 'px';
        map.style.position = 'absolute';
        map.style.top = '0';
        map.style.left = '0';
        map.style.outline = 'none';
        return map
    }

    coverRect(canvasWidth: number, canvasHeight: number, imgWidth: number, imgHeight: number) {
        let x = 0;
        let y = 0;
        let width = imgWidth;
        let height = imgHeight;

        if ((imgWidth / imgHeight) > (canvasWidth / canvasHeight)) {
            width = (canvasWidth / canvasHeight) * imgHeight
            x = (imgWidth - width) / 2
        } else {
            height = (canvasHeight / canvasWidth) * imgWidth;
            y = (imgHeight - height) / 2
        }

        this.ratio = (canvasWidth / width);

        return {
            x: x,
            y: y,
            width: width,
            height: height
        };
    }

    coverCenter(canvasWidth: number, canvasHeight: number, imgWidth: number, imgHeight: number) {
        let x = 0;
        let y = 0;
        let width = imgWidth;
        let height = imgHeight;
        let centerScreenRatio = 3 / 4

        if ((imgWidth / imgHeight) > (canvasWidth / canvasHeight)) {
            this.ratio = (canvasWidth / width);
            y = -(canvasHeight / this.ratio - imgHeight) / 2;
            height = canvasHeight / this.ratio
            x = -(canvasWidth / this.ratio - imgWidth) / 2
        } else {
            this.ratio = (canvasHeight / height);
            x = -(canvasWidth / this.ratio - imgWidth) / 2
            width = canvasWidth / this.ratio
            y = -(canvasHeight / this.ratio - imgHeight) / 2
        }

        return {
            x: x,
            y: y,
            width: width,
            height: height
        };
    }

    private onMouseMove = (e: MouseEvent) => {
        e.preventDefault();
        e.stopPropagation();
        // console.log('onMouseMove', e)
        let ePos = {x: e.x - this.map.getBoundingClientRect().x, y: e.y - this.map.getBoundingClientRect().y}

        this.mapHover(ePos)

        if (this.isDragStart) {
            this.mapMove(ePos)
        }
    }

    private onMouseDown = (e: MouseEvent) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('onMouseDown', e, this.map.getBoundingClientRect())
        this.isDragStart = true;
        this.startDragPos = {x: e.x - this.map.getBoundingClientRect().x, y: e.y - this.map.getBoundingClientRect().y}
    }

    private onMouseUp = (e: MouseEvent) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('onMouseUp', e)
        this.isDragStart = false

        let ePos = {x: e.x - this.map.getBoundingClientRect().x, y: e.y - this.map.getBoundingClientRect().y}
        if (this.mouseMoveType === MouseMoveType.CLICK) {
            this.mapClick(ePos)
        }
        this.mouseMoveType = MouseMoveType.CLICK
    }

    private onMouseLeave = (e: MouseEvent) => {
        e.preventDefault();
        e.stopPropagation();
        console.log('onMouseLeave', e)
        this.isDragStart = false;
    }

    private onWheel = e => {
        e.preventDefault();
        e.stopPropagation();
        // console.log('onWheel', e)
        if (e.deltaY > 0) {
            this.mapTranslate({
                x: e.x - this.map.getBoundingClientRect().x,
                y: e.y - this.map.getBoundingClientRect().y
            }, 0.9)
        } else {
            this.mapTranslate({
                x: e.x - this.map.getBoundingClientRect().x,
                y: e.y - this.map.getBoundingClientRect().y
            }, 1.1)
        }
    }

    public resize() {}
    public winResize() {}

    public _resize() {
        let _w = this.parentElem.clientWidth;
        let _h = this.parentElem.clientHeight;

        if(this.debug) {
            console.log('space-marker resize check', _w, _h)
        }

        if (_w !== this.width || _h !== this.height) {
            requestAnimationFrame(() => {
                this.width = _w;
                this.height = _h;

                if(this.debug) {
                    console.log('space-marker auto resize', _w, _h)
                }

                this.map.width = this.width * this.dpiRatio;
                this.map.height = this.height * this.dpiRatio;
                this.map.style.width = this.width + 'px';
                this.map.style.height = this.height + 'px';
                this.ctx.scale(this.dpiRatio, this.dpiRatio);

                this.mapImgRect = this.resizeModeFns[this.resizeMode](this.width, this.height, this.mapImg.width, this.mapImg.height);

                this.drawMapImage()
            })
        }
    }

    private _winResize = () => {
        this.time = setTimeout(() => {
            this._resize();
            this.time = setTimeout(this._winResize, 500)
        }, 500)
    }

    private onBlur = () => {
        console.log('onBlur')
    }

    private onContextMenu = () => {
        return false;
    }

    private onDblClick = (e: MouseEvent) => {
        console.log('onDblClick')
    }

    private mapMove(pos: Point) {
        let dx = (pos.x - this.startDragPos.x) / this.scaleX;
        let dy = (pos.y - this.startDragPos.y) / this.scaleY;

        if (Math.abs(dx) < this.triggerMoveDistance && Math.abs(dy) < this.triggerMoveDistance) {
            return;
        }

        this.mouseMoveType = MouseMoveType.DRAG;

        this.startDragPos = pos;

        this.mapImgRect.x -= (dx / this.ratio);
        this.mapImgRect.y -= (dy / this.ratio);

        requestAnimationFrame(() => {
            this.drawMapImage();
        })
    }

    private mapClick(pos: Point) {
        let dx = (pos.x - this.startDragPos.x) / this.scaleX;
        let dy = (pos.y - this.startDragPos.y) / this.scaleY;

        if (Math.abs(dx) > this.triggerMoveDistance || Math.abs(dy) > this.triggerMoveDistance) {
            return;
        }
        // 编辑模式
        if (!!this.editModel) {
            if (this.editModel === 'mark') {
                let _editableMark = this.marks.find(f=>f.id === '@editableMark')

                if(_editableMark) {
                    this.updateMark('@editableMark', {
                        visible: true,
                        center: new Point(
                            (pos.x / this.scaleX) / this.ratio + this.mapImgRect.x,
                            (pos.y / this.scaleY) / this.ratio + this.mapImgRect.y,
                        )
                    });

                    this.dispatch('mark_pos', {
                        w: _.get(_editableMark.options,'w'),
                        h: _.get(_editableMark.options,'h'),
                        x: ((_.get(_editableMark.options,'center.x') * _.get(_editableMark.options,'w')) /  this.mapImg.width),
                        y: ((_.get(_editableMark.options,'center.y') * _.get(_editableMark.options,'w')) /  this.mapImg.width),
                    })
                }
            } else if(this.editModel === 'range') {
                // todo
            }
            this.drawMapImage();
            return;
        }


        let markHit = false;
        let hitMark = null;
        for (let i = this.marks.length - 1; i >= 0; i--) {
            if (this.marks[i].hit({
                x: pos.x / this.scaleX,
                y: pos.y / this.scaleY
            }) && !markHit && this.marks[i].visible) {
                markHit = true;
                hitMark = this.marks[i];
            } else {
                this.marks[i].isActive = false;
            }
        }

        if (markHit && hitMark) {
            hitMark.setActive(true);
            this.dispatch('mark_click', hitMark)
        } else {
            this.drawMapImage()
        }

        let rangeHit = false;
        let hitRange = null;
        for (let i = this.ranges.length - 1; i >= 0; i--) {
            if (this.ranges[i].hit({
                x: pos.x / this.scaleX,
                y: pos.y / this.scaleY
            }) && !rangeHit && this.ranges[i].visible) {
                rangeHit = true;
                hitRange = this.ranges[i];
            } else {
                this.ranges[i].isActive = false;
            }
        }

        if (rangeHit && hitRange && !markHit) {
            hitRange.setActive(true);
            this.dispatch('range_click', hitRange)
        } else {
            this.drawMapImage();
        }
    }

    private mapHover(pos: Point) {
        let markHit = false;
        let hitMark = null;
        for (let i = this.marks.length - 1; i >= 0; i--) {
            if (this.marks[i].hit({
                x: pos.x / this.scaleX,
                y: pos.y / this.scaleY
            }) && !markHit && this.marks[i].visible) {
                markHit = true;
                hitMark = this.marks[i];
            }
        }

        if (markHit && hitMark) {
            if (this.hoverMark && this.hoverMark.id !== hitMark.id) {
                this.dispatch('mark_leave', this.hoverMark);
                this.hoverMark = null;
            }

            if (!this.hoverMark || (this.hoverMark && this.hoverMark.id !== hitMark.id)) {
                this.dispatch('mark_enter', hitMark)
                this.hoverMark = hitMark;
            }
        } else {
            if (this.hoverMark) {
                this.dispatch('mark_leave', this.hoverMark);
                this.hoverMark = null;
            }
        }

        let rangeHit = false;
        let hitRange = null;
        for (let i = this.ranges.length - 1; i >= 0; i--) {
            if (this.ranges[i].hit({
                x: pos.x / this.scaleX,
                y: pos.y / this.scaleY
            }) && !rangeHit && this.ranges[i].visible) {
                rangeHit = true;
                hitRange = this.ranges[i];
            }
        }

        if (rangeHit && hitRange && !markHit) {
            if (this.hoverRange && this.hoverRange.id !== hitRange.id) {
                this.dispatch('range_leave', this.hoverRange);
                this.hoverRange = null;
            }

            if (!this.hoverRange || (this.hoverRange && this.hoverRange.id !== hitRange.id)) {
                this.dispatch('range_enter', hitRange)
                this.hoverRange = hitRange;
            }
        } else {
            if (this.hoverRange) {
                this.dispatch('range_leave', this.hoverRange);
                this.hoverRange = null;
            }
        }
    }

    private mapTranslate(pos: Point, ratio: number) {
        pos = {x: pos.x / this.scaleX, y: pos.y / this.scaleY}

        let x0 = pos.x / this.ratio + this.mapImgRect.x;
        let y0 = pos.y / this.ratio + this.mapImgRect.y;

        this.ratio = this.ratio * ratio;

        this.mapImgRect.width = this.width / this.ratio;
        this.mapImgRect.height = this.height / this.ratio;
        this.mapImgRect.x = x0 - pos.x / this.ratio;
        this.mapImgRect.y = y0 - pos.y / this.ratio;

        requestAnimationFrame(() => {
            this.drawMapImage();
        })
    }

    public update(options) {
        _.get(options, 'scaleX') && (this.scaleX = _.get(options, 'scaleX'));
        _.get(options, 'scaleY') && (this.scaleY = _.get(options, 'scaleY'));
    }

    public center(pos: Point) {
        let dx = pos.x - this.width / 2
        let dy = pos.y - this.height / 2

        this.mapImgRect.x += (dx / this.ratio);
        this.mapImgRect.y += (dy / this.ratio);
    }

    private dispatch(event: string, data: any) {
        if (this.options.on) {
            this.options.on(event, data);
        }
    }

    // 重新初始化
    public reset(options) {
        if (this.debug) {
            console.log('space-marker>>>>>>>', 'reset', options)
        }

        this.initialize(options);
    }

    // 更新Mark
    public updateMark(id, options, path: string = 'id') {
        if (this.debug) {
            console.log('space-marker>>>>>>>', 'updateMark', id, options, path)
        }

        this.marks.forEach(m => {
            if (_.get(m, path) == id) {
                m.update(options);
            }
        })

        this.drawMapImage();
    }

    // 更新Range
    public updateRange(id, options, path: string = 'id') {
        if (this.debug) {
            console.log('space-marker>>>>>>>', 'updateRange', id, options, path)
        }

        this.ranges.forEach(m => {
            if (_.get(m, path) == id) {
                m.update(options);
            }
        })

        this.drawMapImage();
    }

    // 高亮Mark
    public setMarkHighlight(id, path: string = 'id') {
        if (this.debug) {
            console.log('space-marker>>>>>>>', 'setMarkHighlight', id, path)
        }

        if (!_.isArray(id)) {
            id = [id]
        }

        id = _.uniq(id);    // 去重
        id = _.compact(id); // 去假值

        this.marks.forEach(_m => {
            _m.highlight = id.includes(_.get(_m, path));
        })

        this.drawMapImage();
    }

    // 显示Mark
    public setMarkVisible(id, path: string = 'id') {
        if (this.debug) {
            console.log('space-marker>>>>>>>', 'setMarkVisible', id, path)
        }

        let _visible = true;
        if (_.isBoolean(id)) {
            _visible = id;
            id = [];
        } else if (!_.isArray(id)) {
            id = [id]
        }

        id = _.uniq(id);    // 去重
        id = _.compact(id); // 去假值

        this.marks.forEach(_m => {
            if (id.length) {
                _m.visible = id.includes(_.get(_m, path));
            } else {
                _m.visible = _visible;
            }
        })

        this.drawMapImage();
    }



    // 设置编辑模式
    public setEditable(type, options) {
        if (['mark', 'range'].includes(type)) {
            this.editModel = type;
            this.marks.push(new Mark({...options, id: '@editableMark', visible: false, w: this.mapImg.width, h: this.mapImg.height}, this))
        } else {
            this.editModel = null;
        }
    }
}
